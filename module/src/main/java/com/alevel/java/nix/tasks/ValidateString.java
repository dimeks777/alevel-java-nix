package com.alevel.java.nix.tasks;

import java.util.ArrayDeque;

public class ValidateString {
    private static final char[] OPENING_BRACKETS = {'(', '[', '{'};
    private static final char[] CLOSING_BRACKETS = {')', ']', '}'};

    private static boolean checkIfClosingBracket(char symbol) {
        for (char closingBracket : CLOSING_BRACKETS) {
            if (symbol == closingBracket) return true;
        }
        return false;
    }

    private static boolean checkIfOpeningBracket(char symbol) {
        for (char openingBracket : OPENING_BRACKETS) {
            if (symbol == openingBracket) return true;
        }
        return false;
    }

    private static boolean checkPairness(char openingBracket, char closingBracket) {
        return (openingBracket == '(' && closingBracket == ')') || (openingBracket == '[' && closingBracket == ']') || (openingBracket == '{' && closingBracket == '}');
    }

    public static boolean checkIfValidate(String str) {
        if (str == null) return false;
        int length = str.length();
        if (length == 0) return true;
        ArrayDeque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < length; i++) {
            char symbol = str.charAt(i);

            if (ValidateString.checkIfClosingBracket(symbol)) {
                if (stack.size() != 0) {
                    if (ValidateString.checkPairness(stack.peek(), symbol)) {
                        stack.pop();
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                if (ValidateString.checkIfOpeningBracket(symbol)) {
                    stack.push(symbol);
                } else {
                    return false;
                }
            }

        }
        return true;
    }

}
