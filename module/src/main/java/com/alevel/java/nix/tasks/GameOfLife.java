package com.alevel.java.nix.tasks;

public class GameOfLife {

    private static final boolean LIVE = true;
    private static final boolean DEAD = false;

    private static class Cell {
        private boolean lifeStatus;
        private short countOfAliveNeighbors;


        public boolean getLifeStatus() {
            return lifeStatus;
        }

        public void setLifeStatus(boolean lifeStatus) {
            this.lifeStatus = lifeStatus;
        }

        public short getCountOfNeighbors() {
            return countOfAliveNeighbors;
        }

        public void setCountOfNeighbors(short countOfAliveNeighbors) {
            this.countOfAliveNeighbors = countOfAliveNeighbors;
        }

        Cell() {

        }

        public static Cell[][] arrayInit(boolean[][] arr, int y, int x) {
            Cell[][] newArr = new Cell[y][x];
            for (int i = 0; i < y; i++) {
                for (int j = 0; j < x; j++) {
                    newArr[i][j] = new Cell();
                }
            }
            return newArr;
        }

        public static boolean[][] convertToBoolean(Cell[][] cells) {
            boolean[][] arr = new boolean[cells.length][cells[0].length];
            for (int i = 0; i < cells.length; i++) {
                for (int j = 0; j < cells[0].length; j++) {
                    arr[i][j] = cells[i][j].getLifeStatus();
                }
            }
            return arr;
        }


    }

    private static int value(int row, int col, Cell[][] cells) {
        boolean edge = row == -1 || col == -1 || row == cells.length || col == cells[0].length;
        return !edge && cells[row][col].getLifeStatus() ? 1 : 0;
    }

    private static short getCountOfAliveNeighbors(Cell[][] board, int row, int col) {

        int up = row - 1;
        int down = row + 1;
        int left = col - 1;
        int right = col + 1;
        return (short) (value(up, right, board) + value(up, col, board) + value(up, left, board) + value(row, right, board)
                + value(row, left, board) + value(down, right, board) + value(down, col, board) + value(down, left, board));

    }

    public static void recalculateNeighbors(Cell[][] cells) {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                cells[i][j].setCountOfNeighbors(GameOfLife.getCountOfAliveNeighbors(cells, i, j));
            }
        }
    }

    private static void nextCycle(Cell[][] cells) {
        for (Cell[] cell : cells) {
            for (int j = 0; j < cells[0].length; j++) {
                short currentCount = cell[j].getCountOfNeighbors();
                boolean currentLifeStatus = cell[j].getLifeStatus();
                if (currentCount < 2 && currentLifeStatus) {
                    cell[j].setLifeStatus(DEAD);
                }
                if ((currentCount == 2 || currentCount == 3) && currentLifeStatus) {
                    cell[j].setLifeStatus(LIVE);
                }
                if (currentCount > 3 && currentLifeStatus) {
                    cell[j].setLifeStatus(DEAD);
                }
                if (currentCount == 3 && !currentLifeStatus) {
                    cell[j].setLifeStatus(LIVE);
                }

            }

        }
        GameOfLife.recalculateNeighbors(cells);
    }

    static void setUp(boolean[][] board, Cell[][] cells) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                cells[i][j].setLifeStatus(board[i][j]);

            }
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                cells[i][j].setCountOfNeighbors(getCountOfAliveNeighbors(cells, i, j));
            }
        }
    }


    public static void printBoard(boolean[][] board) {
        for (boolean[] booleans : board) {
            for (int j = 0; j < board[0].length; j++) {
                if (booleans[j]) {
                    System.out.print(1 + " ");
                } else {
                    System.out.print(0 + " ");
                }

            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }


    public static boolean[][] lifeCycle(boolean[][] board) {
        if (board == null) return null;
        Cell[][] cells = Cell.arrayInit(board, board.length, board[0].length);
        GameOfLife.setUp(board, cells);
        GameOfLife.nextCycle(cells);
        return Cell.convertToBoolean(cells);
    }

}
