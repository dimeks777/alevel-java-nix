package com.alevel.java.nix.tasks;

import java.util.HashSet;

public class ArrayUniqueNumbers {

    public static int getNumberOfUniqueElements(int[] arr) {
        if (arr == null) return 0;
        if (arr.length == 1) return 1;
        HashSet<Integer> uniqueNumbers = new HashSet<Integer>();

        for (int value : arr) {
            uniqueNumbers.add(value);
        }

        return uniqueNumbers.size();
    }

}
