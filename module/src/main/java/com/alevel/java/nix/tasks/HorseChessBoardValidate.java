package com.alevel.java.nix.tasks;

public class HorseChessBoardValidate {

    private static final Point[] POSSIBLE_MOVES = {
            new Point(-3, 1),
            new Point(1, 3),
            new Point(3, -1),
            new Point(-1, -3)
    };

    public static boolean horseValidateMove(Point position, Point newPosition) {
        if (newPosition == null || position == null) return false;
        for (Point possibleMove : POSSIBLE_MOVES) {
            if (Point.equals(newPosition, position.move(new Point(position), possibleMove))) return true;
        }
        return false;
    }


}
