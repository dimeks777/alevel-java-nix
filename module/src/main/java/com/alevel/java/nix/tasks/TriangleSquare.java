package com.alevel.java.nix.tasks;

public class TriangleSquare {

    public static double getSquare(Point a, Point b, Point c) {
        if (a == null || b == null || c == null) return 0;
        if (Point.equals(a, b) || Point.equals(b, c) || Point.equals(a, c)) return 0;
        double square = Math.abs((a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY()))/2.0);
        square = square * 10;
        square = Math.round(square);
        square /= 10;
        return square;
    }
}

