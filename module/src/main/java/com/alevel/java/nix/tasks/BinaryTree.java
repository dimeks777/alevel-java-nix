package com.alevel.java.nix.tasks;


public class BinaryTree {
    Node root;

    static class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
            right = null;
            left = null;
        }
    }

    private Node addRecursive(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }

        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        } else {
            return current;
        }

        return current;
    }

    public void add(int value) {
        root = addRecursive(root, value);
    }


    int getMaxDepth(Node node, int depth) {
        if (node == null) return depth;
        return Math.max(getMaxDepth(node.left, depth + 1), getMaxDepth(node.right, depth + 1));
    }

    public BinaryTree createBinaryTree(int[] values) {
        if (values == null || values.length == 0) return null;
        BinaryTree tree = new BinaryTree();
        tree.add(values[0]);
        if (values.length == 1) {
            return tree;
        }

        for (int i = 1; i < values.length; i++) {
            tree.add(values[i]);
        }
        return tree;
    }

    public static int findMaxDepth(int[] arr) {
        int maxCount = 0;
        BinaryTree tree = new BinaryTree().createBinaryTree(arr);
        if (tree == null) return 0;
        maxCount = tree.getMaxDepth(tree.root, maxCount);
        return maxCount;
    }

}
