package com.alevel.java.nix.tasks;

public class Point {
    private int x;
    private int y;

    Point(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static boolean equals(Point first, Point second) {
        return (first.getX() == second.getX() && first.getY() == second.getY());
    }

    public Point move(Point position, Point coordinates) {
        position.setX(position.getX() + coordinates.getX());
        position.setY((position.getY() + coordinates.getY()));
        return position;
    }

}
