package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameOfLifeTest {

    @Test
    void testGameOfLife() {
        assertGameOfLife(new boolean[][]{
                {true, false, true, false, true},
                {true, true, false, false, false},
                {false, true, true, true, false},
                {true, true, false, true, true},
                {false, true, false, true, true}

        }, new boolean[][]{
                {true, false, false, false, false},
                {true, false, false, false, false},
                {false, false, false, true, true},
                {true, false, false, false, false},
                {true, true, false, true, true}
        });
        assertGameOfLife(null, null);
        assertGameOfLife(new boolean[][]{{true}}, new boolean[][]{{false}});
        assertGameOfLife(new boolean[][]{{true}, {true}, {true}, {true}, {true}}, new boolean[][]{{false}, {true}, {true}, {true}, {false}});
        assertGameOfLife(new boolean[][]{{true, true, true, true, true}}, new boolean[][]{{false, true, true, true, false}});
        assertGameOfLife(new boolean[][]{{true, true}, {true, true}}, new boolean[][]{{true, true}, {true, true}});
    }

    void assertGameOfLife(boolean[][] base, boolean[][] expected) {
        boolean[][] actual = GameOfLife.lifeCycle(base);
        if (actual == null) {
            assertNull(expected);
        }
        assertArrayEquals(expected, actual);
    }

}