package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeTest {

    @Test
    void testFindMaxDepth() {
        assertFindMaxDepth(new int[]{1, 22, 3, 14, 36, 5, 7, 28}, 6);
        assertFindMaxDepth(null, 0);
        assertFindMaxDepth(new int[]{1}, 1);
        assertFindMaxDepth(new int[]{}, 0);
    }

    private void assertFindMaxDepth(int[] values, int expected) {
        int actual = BinaryTree.findMaxDepth(values);
        assertEquals(expected, actual);
    }

}