package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateStringTest {

    @Test
    void testValidateString(){
        assertValidateString("Not valid",false);
        assertValidateString(null,false);
        assertValidateString("",true);
        assertValidateString("{([])}",true);
        assertValidateString("}",false);
        assertValidateString("{{})",false);
        assertValidateString("{{{([])}}{}}",true);

    }

    private void assertValidateString(String base, boolean expected) {
        boolean actual = ValidateString.checkIfValidate(base);
        assertEquals(expected,actual);
    }

}