package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayUniqueNumbersTest {


    @Test
    public void testArrayUniqueNumbers(){
        assertUniqueNumbers(new int[]{1,1,1,1,1,1},1);
        assertUniqueNumbers(new int[]{1,2,3,4,5,6},6);
        assertUniqueNumbers(null,0);
        assertUniqueNumbers(new int[]{0},1);
    }

    private void assertUniqueNumbers(int[] arr, int expected) {
        int actual = ArrayUniqueNumbers.getNumberOfUniqueElements(arr);
        assertEquals(expected,actual);
    }


}