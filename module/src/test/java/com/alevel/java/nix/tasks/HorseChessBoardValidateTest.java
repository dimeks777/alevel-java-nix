package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HorseChessBoardValidateTest {

    @Test
    void testHorseValidateMove() {
        assertMove(new Point(5, 5), new Point(1,1),false);
        assertMove(new Point(5, 5), new Point(2,6),true);
        assertMove(new Point(0, 0), new Point(3,-1),true);
        assertMove(null, null,false);

    }

    void assertMove(Point basePosition, Point newPosition, boolean expected){
        boolean actual = HorseChessBoardValidate.horseValidateMove(basePosition,newPosition);
        assertEquals(expected,actual);
    }



}