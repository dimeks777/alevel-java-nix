package com.alevel.java.nix.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleSquareTest {

    @Test
    void testGetTriangleSquare() {
        assertSquare(new Point(0, 0), new Point(7, 5), new Point(3, 2), 0.5);
        assertSquare(null, null, null, 0);
        assertSquare(new Point(1, 1), new Point(2, 3), new Point(3, 1), 2);
    }

    void assertSquare(Point a, Point b, Point c, double expected) {
        double actual = TriangleSquare.getSquare(a, b, c);
        assertEquals(expected, actual);
    }
}