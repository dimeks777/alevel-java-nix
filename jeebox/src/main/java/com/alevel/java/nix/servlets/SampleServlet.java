package com.alevel.java.nix.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet(name = "sample-servlet", urlPatterns = "/sample")
public class SampleServlet extends HttpServlet {
    private final Map<String, String> clients = new ConcurrentHashMap<>();


    private static final Logger log = LoggerFactory.getLogger(SampleServlet.class);

    @Override
    public void init() {
        log.info("Sample Servlet initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter responseBody = resp.getWriter();

        resp.setContentType("text/html");
        clients.put(req.getHeader("User-Agent"), req.getRemoteHost());
        StringBuilder stringBuilder = new StringBuilder();
        for (var e : clients.entrySet()) {
            stringBuilder.append("<b>").append(e.getValue()).append("::").append(e.getKey()).append("<br>").append("</b>");
        }
        responseBody.println("<h1 align=\"center\">Unique users: <br> </h1>");
        responseBody.println(stringBuilder);

        String client = req.getParameter("client");
        if (client == null) {
            client = "anonymous user";
        }

        responseBody.println("<h3 align=\"center\">Hi, " + client  + ". Your local ip is: "+ req.getRemoteHost() + " </h3>");
    }

    @Override
    public void destroy() {
        log.info("Sample Servlet destroyed");
    }
}
