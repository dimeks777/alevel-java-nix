package com.alevel.java.nix.tasks.task3;

import java.util.*;

public class SimpleDijkstra {

    private final List<City> cities;
    private final Map<Integer, Integer> neededPaths;

    public SimpleDijkstra(List<City> cities, Map<Integer, Integer> neededPaths) {
        this.cities = cities;
        this.neededPaths = neededPaths;
    }


    public static void getAllPaths(City source) {

        source.setMinCost(0);
        PriorityQueue<City> cityPriorityQueue = new PriorityQueue<>();
        cityPriorityQueue.add(source);
        while (!cityPriorityQueue.isEmpty()) {
            City src = cityPriorityQueue.poll();

            for (Road road : src.getRoads()) {
                City dest = road.getDest();
                int cost = road.getCost();
                int currentCost = src.getMinCost() + cost;
                if (currentCost < dest.getMinCost()) {
                    cityPriorityQueue.remove(dest);
                    dest.setMinCost(currentCost);
                    cityPriorityQueue.add(dest);
                }
            }
        }
    }

    private int findShortestPath(City src, City dest) {
        getAllPaths(src);
        return dest.getMinCost();
    }

    public List<Integer> result() {
        List<Integer> list = new ArrayList<>();
        for (var entry : neededPaths.entrySet()) {
            list.add(findShortestPath(cities.get(entry.getKey()), cities.get(entry.getValue())));
        }
        return list;
    }

}

