package com.alevel.java.nix.tasks.task1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DateFilter {

    private static final List<DateTimeFormatter> patterns = List.of(DateTimeFormatter.ofPattern("yyyy/MM/dd"), DateTimeFormatter.ofPattern("dd/MM/yyyy"), DateTimeFormatter.ofPattern("MM-dd-yyyy"));
    private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");

    public static Collection<String> filter(Collection<String> list) {
        List<String> newList = new ArrayList<>();
        String temp;
        for (String date : list) {
            if ((temp = parse(date)) != null) {
                newList.add(temp);
            }
        }
        return newList;
    }

    public static void printList(Collection<String> list) {
        for (String s : list) {
            System.out.println(s);
        }
    }

    private static String compress(LocalDate localDate) {
        return localDate.format(format);
    }

    private static String parse(String date) {
        boolean acceptable = false;
        LocalDate localDate = null;
        for (DateTimeFormatter pattern : patterns) {
            try {
                localDate = LocalDate.parse(date, pattern);
                acceptable = true;
                break;
            } catch (DateTimeParseException ignored) {

            }
        }

        if (acceptable) {
            return compress(localDate);
        }
        return null;
    }

}
