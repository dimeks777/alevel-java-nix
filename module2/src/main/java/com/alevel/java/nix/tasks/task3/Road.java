package com.alevel.java.nix.tasks.task3;

public class Road {

    private final City dest;
    private final int cost;

    public Road(City dest, int cost) {
        this.dest = dest;
        this.cost = cost;
    }

    public City getDest() {
        return dest;
    }

    public int getCost() {
        return cost;
    }
}
