package com.alevel.java.nix.tasks.task3;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        SimpleDijkstra simpleDijkstra = IO.read("/input.txt");
        if (simpleDijkstra != null) {
            List<Integer> list = simpleDijkstra.result();
            IO.printResult(list);
        }
    }
}
