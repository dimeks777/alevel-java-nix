package com.alevel.java.nix.tasks.task3;

import java.util.List;

public class City implements Comparable<City> {

    private final long id;

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private List<Road> roads;
    private int minCost = Integer.MAX_VALUE;

    public String toString() {
        return name;
    }

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public City(int id, String name, List<Road> roads) {
        this.id = id;
        this.name = name;
        this.roads = roads;
    }

    public long getId() {
        return id;
    }

    public void setRoads(List<Road> roads) {
        this.roads = roads;
    }

    public String getName() {
        return name;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public List<Road> getRoads() {
        return roads;
    }

    public int getMinCost() {
        return minCost;
    }

    @Override
    public int compareTo(City other) {
        return Integer.compare(this.minCost, other.minCost);
    }


}
