package com.alevel.java.nix.tasks.task3;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class IO {

    public static SimpleDijkstra read(String cp) {
        List<String> data = null;
        try {
            Path fileURL = Path.of(IO.class.getResource(cp).toURI());
            data = Files.readAllLines(fileURL);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        if (data != null) {

            int size = Integer.parseInt(data.get(0));
            List<City> cities = new ArrayList<>(size);
            HashMap<String, Integer> names = new HashMap<>();
            for (int i = 1; i < size + 1; i++) {
                cities.add(new City(i, null));
            }
            int step;
            int last = 0;

            for (int i = 1, count = 0; i < data.size() && count < size; i += step, count++) {

                String temp = data.get(i);
                int countOfRoads = Integer.parseInt(data.get(i + 1));
                step = countOfRoads + 2;
                List<Road> edges = new ArrayList<>();
                for (int j = 0; j < countOfRoads; j++) {
                    String[] tempEdges = data.get(i + 2 + j).split(" ");
                    edges.add(new Road(cities.get(Integer.parseInt(tempEdges[0]) - 1), Integer.parseInt(tempEdges[1])));
                }
                cities.get(count).setRoads(edges);
                cities.get(count).setName(temp);
                names.put(temp, count);
                if (count == size - 1) {
                    last = i + step;
                }
            }

            int neededPaths = Integer.parseInt(data.get(last));
            Map<Integer, Integer> map = new LinkedHashMap<>();
            for (int i = 1; i < neededPaths + 1; i++) {
                String[] srcDest = data.get(last + i).split(" ");
                map.put(names.get(srcDest[0]), names.get(srcDest[1]));
            }
            return new SimpleDijkstra(cities, map);
        }
        return null;
    }


    public static void printResult(List<Integer> list) {
        try {
            Path fileURL = Path.of(IO.class.getResource("/output.txt").toURI());
            FileWriter fileWriter = new FileWriter(fileURL.toFile());
            PrintWriter printWriter = new PrintWriter(fileWriter);

            for (Integer integer : list) {
                printWriter.println(integer);
            }
            fileWriter.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
