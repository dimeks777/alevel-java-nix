package com.alevel.java.nix.tasks.task3;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class SimpleDijkstraTest {

    @Test
    void result() {

        City gdansk = new City(1, "gdansk");
        City bydgoszcz = new City(2,"bydgoszcz");
        City torun =  new City(3,"torun");
        City warszawa =  new City(4,"warszawa");

        gdansk.setRoads(List.of(
                new Road(bydgoszcz, 1),
                new Road(torun, 3)));
        bydgoszcz.setRoads(List.of(new Road(gdansk, 1),
                new Road(torun, 1),
                new Road(warszawa, 4)
        ));
        torun.setRoads(List.of(new Road(gdansk, 3),
                new Road(bydgoszcz, 1),
                new Road(warszawa, 1)
        ));
        warszawa.setRoads(List.of(new Road(gdansk, 4),
                new Road(torun, 1)
        ));

        SimpleDijkstra simpleDijkstra = new SimpleDijkstra(List.of(gdansk,bydgoszcz,torun,warszawa), Map.of(0,3,1,3));
        assertResult(simpleDijkstra,List.of(3,2));
    }

    private void assertResult(SimpleDijkstra simpleDijkstra, List<Integer> expected) {
        assertEquals(expected, simpleDijkstra.result());
    }
}