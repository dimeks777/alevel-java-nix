package com.alevel.java.nix.tasks.task1;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DateFilterTest {

    @Test
    void filter() {
        assertFilter(List.of("2020/04/05", "05/12/2020", "1111-11-11", "2012-02-31", "date", "12-05-2044"), List.of("20200405", "20201205", "20441205"));
    }

    private void assertFilter(Collection<String> list, Collection<String> expected) {
        assertEquals(expected, DateFilter.filter(list));
    }
}