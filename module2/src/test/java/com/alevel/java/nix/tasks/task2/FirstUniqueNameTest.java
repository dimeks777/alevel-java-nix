package com.alevel.java.nix.tasks.task2;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FirstUniqueNameTest {

    @Test
    void testFirstUniqueName() {
        assertFirstUniqueName(List.of("Dmitry", "Sergey", "Daniil", "Alex", "Dmitry", "Sergey", "Daniil", "Semyon"), "Alex");
        assertFirstUniqueName(List.of("Apple","Planet","Apple","Planet"), null);
    }

    private void assertFirstUniqueName(Collection<String> list, String expected) {
        assertEquals(expected,FirstUniqueName.of(list));
    }
}