package com.alevel.java.nix.todolistspring.controller;

import com.alevel.java.nix.todolistspring.entity.Action;
import com.alevel.java.nix.todolistspring.entity.request.SaveActionRequest;
import com.alevel.java.nix.todolistspring.exception.ActionNotFoundException;
import com.alevel.java.nix.todolistspring.service.TodoCRUD;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/todo")
public class TodoController {

    private final TodoCRUD todoCRUD;

    public TodoController(TodoCRUD todoCRUD) {
        this.todoCRUD = todoCRUD;
    }

    @GetMapping("/{id}")
    public Action get(@PathVariable UUID id) {
        return todoCRUD.getById(id)
                .orElseThrow(() -> new ActionNotFoundException(id));
    }

    @GetMapping("/not_done")
    public List<Action> getNotDone() {
        return todoCRUD.getNotDone();
    }

    @GetMapping("/all")
    public List<Action> getAll() {
        return todoCRUD.getAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Action create(@RequestBody SaveActionRequest request) {
        return todoCRUD.create(request);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void update(@PathVariable UUID id, @RequestBody SaveActionRequest request) {
        todoCRUD.update(id, request);
    }

    @DeleteMapping("/{id}")
    public Action delete(@PathVariable UUID id) {
        return todoCRUD.deleteById(id)
                .orElseThrow(() -> new ActionNotFoundException(id));

    }
}
