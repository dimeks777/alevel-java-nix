package com.alevel.java.nix.todolistspring.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "actions")
public class Action {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private boolean done;

    public Action() {

    }

    public Action(String text, boolean done) {
        this.text = text;
        this.done = done;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
