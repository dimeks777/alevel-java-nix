package com.alevel.java.nix.todolistspring.service;

import com.alevel.java.nix.todolistspring.entity.Action;
import com.alevel.java.nix.todolistspring.entity.request.SaveActionRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TodoCRUD {
    Action create(SaveActionRequest request);
    List<Action> getAll();
    Optional<Action> getById(UUID id);
    List<Action> getNotDone();
    void update(UUID id, SaveActionRequest request);
    Optional<Action> deleteById(UUID id);
}
