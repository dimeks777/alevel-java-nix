package com.alevel.java.nix.todolistspring.repository;

import com.alevel.java.nix.todolistspring.entity.Action;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TodoRepository extends JpaRepository<Action, UUID> {
    List<Action> findByDoneIsFalse();
}
