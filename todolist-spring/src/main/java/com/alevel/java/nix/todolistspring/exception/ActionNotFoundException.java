package com.alevel.java.nix.todolistspring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;


import java.util.UUID;

public class ActionNotFoundException extends ResponseStatusException {

    public ActionNotFoundException(UUID id) {
        super(HttpStatus.NOT_FOUND,"Action with id " + id + " was not found");
    }
}
