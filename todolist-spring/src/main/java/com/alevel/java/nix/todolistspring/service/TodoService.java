package com.alevel.java.nix.todolistspring.service;

import com.alevel.java.nix.todolistspring.entity.Action;
import com.alevel.java.nix.todolistspring.entity.request.SaveActionRequest;
import com.alevel.java.nix.todolistspring.exception.ActionNotFoundException;
import com.alevel.java.nix.todolistspring.repository.TodoRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class TodoService implements TodoCRUD {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public Action create(SaveActionRequest request) {
        var action = new Action();
        action.setText(request.getText());
        action.setDone(request.isDone());
        return todoRepository.save(action);
    }

    @Override
    public List<Action> getAll() {
        return todoRepository.findAll();
    }

    @Override
    public Optional<Action> getById(UUID id) {
        return todoRepository.findById(id);
    }

    @Override
    public List<Action> getNotDone() {
        return todoRepository.findByDoneIsFalse();
    }

    @Override
    public void update(UUID id, SaveActionRequest request) {
        var action = todoRepository.findById(id).orElseThrow(()->new ActionNotFoundException(id));
        action.setText(request.getText());
        action.setDone(request.isDone());
        todoRepository.save(action);
    }

    @Override
    public Optional<Action> deleteById(UUID id) {
        var action = todoRepository.findById(id);
        action.ifPresent(todoRepository::delete);
        return action;
    }
}
