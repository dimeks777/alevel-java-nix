package com.alevel.java.nix.hometask.lesson7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseCharSequenceSimpleTest {
    @Test
    void testReverseCharSequenceSimple() {
        assertReverse("abc", new ReverseCharSequenceSimple("cba".toCharArray()));
        assertReverse(null, null);
        assertReverse("a", new ReverseCharSequenceSimple("a".toCharArray()));
        assertReverse("", new ReverseCharSequenceSimple("".toCharArray()));
    }

    private void assertReverse(CharSequence base, ReverseCharSequenceSimple expected) {
        ReverseCharSequenceSimple actual = ReverseCharSequenceSimple.reverse(base);
        if (actual == null) {
            assertNull(expected);
        } else {
            assertEquals(expected.toString(), actual.toString());
        }
    }
}