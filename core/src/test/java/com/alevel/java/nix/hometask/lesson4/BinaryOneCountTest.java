package com.alevel.java.nix.hometask.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryOneCountTest {
    private BinaryOneCount binaryOneCount;

    @BeforeEach
    void setUp() {
        binaryOneCount = new BinaryOneCount();
    }


    @Test
    void testCountOneBinary() {
        assertBinaryOneCount(-1, 64);
        assertBinaryOneCount(31, 5);
        assertBinaryOneCount(0, 0);
    }

    void assertBinaryOneCount(long number, int expected) {
        assertEquals(expected, binaryOneCount.count(number));
    }


}