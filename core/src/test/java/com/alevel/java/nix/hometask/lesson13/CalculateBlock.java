package com.alevel.java.nix.hometask.lesson13;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class CalculateBlock implements Block {

    private static final Logger logger = LoggerFactory.getLogger(CalculateBlock.class);
    private final int upperBound;
    private final int lowerBound;

    public CalculateBlock(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    public void run() throws Exception {
        Random random = new Random();
        int a = random.nextInt(upperBound) + lowerBound;
        int b = random.nextInt(upperBound) + lowerBound;
        try {
            int result = a / b;
            logger.info("Result of calculating a/b (a = {}, b = {}) - {}", a, b, result);
        } catch (Exception e) {
            logger.error("a = {}, b = {}", a, b, new Exception(e.getMessage()));
            throw new Exception(e.getMessage());
        }


    }

}
