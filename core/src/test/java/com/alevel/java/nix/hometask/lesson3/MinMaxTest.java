package com.alevel.java.nix.hometask.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MinMaxTest {
    private MinMax minMax;

    @BeforeEach
    void setUp() {
        minMax = new MinMax();
    }

    @Test
    void testMinMax() {
        assertMinInt(new int[]{6, 9, 3, 2, 5, 1, 7}, 1);
        assertMinInt(new int[]{-1, -10, -4, 0, 10, -8}, -10);
        assertMinInt(new int[]{726, 425, 711, 372, 911}, 372);
        assertMinDouble(new double[]{15.1, 27.32, 9.18, 23.1, 30.96}, 9.18);
        assertMaxInt(new int[]{6, 9, 3, 2, 5, 1, 7}, 9);
        assertMaxInt(new int[]{-1, -10, -4, 0, 10, -8}, 10);
        assertMaxInt(new int[]{726, 425, 711, 372, 911}, 911);
        assertMaxDouble(new double[]{15.1, 27.32, 9.18, 23.1, 30.96}, 30.96);
    }

    private void assertMinInt(int[] arr, int expectedMin) {
        int actual = minMax.minInt(arr);
        assertEquals(expectedMin, actual);
    }

    private void assertMinDouble(double[] arr, double expectedMin) {
        double actual = minMax.minDouble(arr);
        assertEquals(expectedMin, actual);
    }


    private void assertMaxInt(int[] arr, int expectedMax) {
        int actual = minMax.maxInt(arr);
        assertEquals(expectedMax, actual);
    }

    private void assertMaxDouble(double[] arr, double expectedMax) {
        double actual = minMax.maxDouble(arr);
        assertEquals(expectedMax, actual);
    }

}