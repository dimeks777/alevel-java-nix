package com.alevel.java.nix.hometask.lesson12.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubstanceTest {

    @Test
    void testWaterHeatUp() {
        assertWaterHeatUp(80, State.GASEOUS);
        assertWaterHeatUp(-30, State.HARD);
        assertWaterHeatUp(30, State.LIQUID);
    }

    @Test
    void testOxygenHeatUp() {
        assertOxygenHeatUp(-240, State.HARD);
        assertOxygenHeatUp(-220, State.LIQUID);
        assertOxygenHeatUp(1000, State.GASEOUS);
    }

    @Test
    void testIronHeatUp() {
        assertIronHeatUp(-2400, State.HARD);
        assertIronHeatUp(0, State.HARD);
        assertIronHeatUp(2000, State.LIQUID);
        assertIronHeatUp(3000, State.GASEOUS);
    }

    private void assertIronHeatUp(double temperature, State expected) {
        Substance iron = new Iron();
        State actual = iron.heatUp(temperature);
        assertEquals(expected, actual);
    }

    private void assertOxygenHeatUp(double temperature, State expected) {
        Substance oxygen = new Oxygen();
        State actual = oxygen.heatUp(temperature);
        assertEquals(expected, actual);
    }


    private void assertWaterHeatUp(double temperature, State expected) {
        Substance water = new Water();
        State actual = water.heatUp(temperature);
        assertEquals(expected, actual);
    }
}