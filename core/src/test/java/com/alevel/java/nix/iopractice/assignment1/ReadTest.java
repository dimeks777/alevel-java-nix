package com.alevel.java.nix.iopractice.assignment1;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ReadTest {

    @Test
    void readLinesHaveSubString() {
        assertReadLinesHaveSubString("C:\\Users\\dimek\\alevel-java-nix\\core\\log\\test.txt", "sub",
                new ArrayList<>() {
                    {
                        add("6sub");
                        add("9sub");
                        add("unsub");
                    }
                });
        assertReadLinesHaveSubString(null, "1", null);

    }

    private void assertReadLinesHaveSubString(String filename, CharSequence substring, Collection<String> expected) {
        Collection<String> actual = Read.readLinesHaveSubString(filename, substring);
        assertEquals(expected,actual);
    }
}