package com.alevel.java.nix.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistinctAggregatorTest<T> {

    @Test
    void aggregate() {
        assertDistinctInteger(new Integer[]{1, 2, 3, 4, 5}, 5);
        assertDistinctInteger(null, null);
        assertDistinctInteger(new Integer[]{1, 1, 1, 1, 1}, 1);
        assertDistinctString(new String[]{"One", "Two", "Three"}, 3);
        assertDistinctString(null, null);
    }

    private void assertDistinctInteger(Integer[] arr, Integer expected) {
        DistinctAggregator<Integer> distinctAggregator = new DistinctAggregator<>();
        Integer actual = distinctAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

    private void assertDistinctString(String[] arr, Integer expected) {
        DistinctAggregator<String> distinctAggregator = new DistinctAggregator<>();
        Integer actual = distinctAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }
}