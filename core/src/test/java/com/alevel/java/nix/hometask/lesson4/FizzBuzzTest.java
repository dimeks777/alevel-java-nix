package com.alevel.java.nix.hometask.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    private FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp() {
        fizzBuzz = new FizzBuzz();
    }


    @Test
    void testLeftToRight() {
        assertGetStringRepresentationByDivisorLeft(267, "fizz\nfizzbuzz\n7\n");
        assertGetStringRepresentationByDivisorLeft(0, "fizzbuzz\n");
        assertGetStringRepresentationByDivisorLeft(-48587463, "fizz\nfizz\n5\nfizz\n7\nfizz\nfizzbuzz\nbuzz\n");
    }


    @Test
    void testRightToLeft() {
        assertGetStringRepresentationByDivisorRight(267, "7\nfizzbuzz\nfizz\n");
        assertGetStringRepresentationByDivisorRight(0, "fizzbuzz\n");
        assertGetStringRepresentationByDivisorRight(-48587463, "buzz\nfizzbuzz\nfizz\n7\nfizz\n5\nfizz\nfizz\n");
    }

    void assertGetStringRepresentationByDivisorLeft(int number, String expected) {
        assertEquals(expected, fizzBuzz.getStringRepresentationByDivisorLeft(number));
    }


    void assertGetStringRepresentationByDivisorRight(int number, String expected) {
        assertEquals(expected, fizzBuzz.getStringRepresentationByDivisorRight(number));
    }
}