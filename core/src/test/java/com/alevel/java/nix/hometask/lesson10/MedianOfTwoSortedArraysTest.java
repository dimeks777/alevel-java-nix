package com.alevel.java.nix.hometask.lesson10;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MedianOfTwoSortedArraysTest {

    @Test
    void testMedianOfTwoSortedArrays() {
        assertMedian(new int[]{2, 6, 12}, new int[]{1, 3, 5, 10, 15, 25}, 6);
        assertMedian(null, null, 0);
        assertMedian(new int[]{1, 3, 5}, null, 3);
        assertMedian(new int[]{-1, 2}, new int[]{-3, 4}, 0.5);
        assertMedian(new int[]{10000}, new int[]{-1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, 10);
    }

    private void assertMedian(int[] arr1, int[] arr2, double expected) {
        double actual = MedianOfTwoSortedArrays.findMedianSortedArrays(arr1, arr2);
        assertEquals(expected, actual);
    }

}