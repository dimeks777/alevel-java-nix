package com.alevel.java.nix.premodule.task2.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClassicHangmanTest {

    @Test
    void encode() {
        assertEncode("word", "w**d");
        assertEncode("bar", "b*r");
    }

    private void assertEncode(String word, String expected) {
        ClassicHangman classicHangman = new ClassicHangman(word, word, 0);
        assertEquals(expected, classicHangman.encode(classicHangman.getAdjustedWord()));
    }

    @Test
    void hasSymbol() {
        assertHasSymbol("Tennis", 'r', false);
        assertHasSymbol("Tank", 't', true);
    }

    private void assertHasSymbol(String word, char c, boolean expected) {
        ClassicHangman classicHangman = new ClassicHangman(word, word, 0);
        assertEquals(expected, classicHangman.hasSymbol(c));
    }


    @Test
    void decode() {
        assertDecode("Phenomenon", 'e', "Ph*n*m*non", "Phen*menon");
        assertDecode("Orange", 'a', "Or*nge", "Orange");
    }

    private void assertDecode(String word, char c, String encoded, String expected) {
        ClassicHangman classicHangman = new ClassicHangman(word, encoded, 0);
        assertEquals(expected, classicHangman.decode(c));
    }

    @Test
    void result() {
        assertResult("Mouse", "M***e", 't', 5, HangmanStates.SIX_ERRORS);
        assertResult("Sabaton", "Sa*a**n", 's', 0, HangmanStates.NO_ERRORS);
    }

    private void assertResult(String word, String encoded, char in, int errors, String expected) {
        ClassicHangman classicHangman = new ClassicHangman(word, encoded, errors);
        assertEquals(expected, classicHangman.result(in));
    }
}