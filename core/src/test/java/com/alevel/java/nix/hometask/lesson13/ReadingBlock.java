package com.alevel.java.nix.hometask.lesson13;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.util.ArrayDeque;

public class ReadingBlock implements Block {
    private static final Logger logger = LoggerFactory.getLogger(ReadingBlock.class);
    private final String fileName;

    public ReadingBlock(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public void run() throws Exception {
        try {
            FileReader reader = new FileReader(fileName);
            ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();
            int c;
            while ((c = reader.read()) != -1) {
                arrayDeque.push(c);
            }
            logger.info("Reading successful. Count of read symbols: {} ", arrayDeque.size());
        } catch (Exception e) {
            logger.error(String.valueOf(new Exception(e.getMessage())));
            throw new Exception(e.getMessage());
        }

    }

}
