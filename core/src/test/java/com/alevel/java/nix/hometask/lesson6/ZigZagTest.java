package com.alevel.java.nix.hometask.lesson6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZigZagTest {

    @Test
    void testZigZagConversion() {
        assertZigZagConversion("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR");
        assertZigZagConversion("PAYPALISHIRING", 4, "PINALSIGYAHRPI");
        assertZigZagConversion(null, 5, null);
        assertZigZagConversion("", 2, "");
    }

    private void assertZigZagConversion(String base, int numRows, String expected) {
        String actual = ZigZag.convert(base, numRows);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

}