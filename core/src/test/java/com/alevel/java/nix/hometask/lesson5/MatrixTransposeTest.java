package com.alevel.java.nix.hometask.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTransposeTest {
    MatrixTranspose matrixTranspose;

    @BeforeEach
    void setUp() {
        matrixTranspose = new MatrixTranspose();
    }

    @Test
    void testTransposeMatrix() {
        assertTransposeMatrix
                (new int[][]{
                        {1, 2, 3, 10},
                        {4, 5, 6, 11},
                        {7, 8, 9, 12}
                }, new int[][]{
                        {1, 4, 7},
                        {2, 5, 8},
                        {3, 6, 9},
                        {10, 11, 12}
                });
        assertTransposeMatrix
                (new int[][]{
                        {0, 1, 2},
                        {3, 4, 5},
                        {6, 7, 8}
                }, new int[][]{
                        {0, 3, 6},
                        {1, 4, 7},
                        {2, 5, 8}
                });
        assertTransposeMatrix(null, null);
    }

    private void assertTransposeMatrix(int[][] arr, int[][] expected) {
        int[][] actual = matrixTranspose.transposeMatrix(arr);
        if (actual == null) {
            assertNull(expected);
        }
        assertArrayEquals(expected, actual);
    }

}