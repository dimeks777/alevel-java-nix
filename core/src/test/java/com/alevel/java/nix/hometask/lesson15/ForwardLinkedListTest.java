package com.alevel.java.nix.hometask.lesson15;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ForwardLinkedListTest {

    private ForwardLinkedList<String> list;

    @BeforeEach
    void setUp() {
        list = new ForwardLinkedList<>();
        list.add("First");
        list.add("Second");
        list.add("Third");
    }

    @Test
    void get() {
        assertEquals("Second", list.get(1));
    }

    @Test
    void getFirst() {
        assertEquals("First", list.getFirst());
    }

    @Test
    void getLast() {
        assertEquals("Third", list.getLast());
    }

    @Test
    void addFront() {
        int sizeBefore = list.size();
        list.addFront("Fourth");
        int sizeAfter = list.size();
        assertEquals("Fourth", list.getFirst());
        assertEquals(sizeBefore, sizeAfter - 1);
    }

    @Test
    void addBack() {
        int sizeBefore = list.size();
        list.addBack("Fourth");
        int sizeAfter = list.size();
        assertEquals("Fourth", list.getLast());
        assertEquals(sizeBefore, sizeAfter - 1);
    }

    @Test
    void addByIndex() {
        int sizeBefore = list.size();
        list.addByIndex("New", 2);
        int sizeAfter = list.size();
        assertEquals("New", list.get(2));
        assertEquals(sizeBefore, sizeAfter - 1);
    }

    @Test
    void deleteByIndex() {
        int sizeBefore = list.size();
        String deleted = list.deleteByIndex(1);
        int sizeAfter = list.size();
        assertEquals("Second", deleted);
        assertEquals(sizeBefore, sizeAfter + 1);
    }

    @Test
    void remove() {
        assertFalse(list.remove("Apple"));
        assertTrue(list.remove("Third"));
        assertEquals("Second", list.getLast());
    }

    @Test
    void containsElem() {
        assertFalse(list.containsElem(null));
        assertTrue(list.containsElem("First"));

    }

    @Test
    void clear() {
        list.clear();
        ForwardLinkedList<String> newList = new ForwardLinkedList<>();
        assertEquals(list, newList);
    }


    @Test
    void size() {
        int sizeBefore = list.size();
        list.clear();
        int sizeAfter = list.size();
        assertEquals(sizeAfter + sizeBefore, sizeBefore);
    }

    @Test
    void isEmpty() {
        assertFalse(list.isEmpty());
        list.clear();
        assertTrue(list.isEmpty());
    }
}