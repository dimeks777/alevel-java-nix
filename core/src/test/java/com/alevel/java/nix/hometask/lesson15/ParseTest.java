package com.alevel.java.nix.hometask.lesson15;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParseTest {


    @Test
    void testParseDecimalsFromString() {
        List<String> testingList = List.of("No", " digits", " here");
        assertParseDecimalsFromStringException(testingList, new IllegalArgumentException(Parse.errorIllegalArgumentsMessage + testingList.toString()));

        assertParseDecimalsFromStringException(null, new NullPointerException(Parse.errorNullPointerMessage));

        testingList = List.of("Tel 012-34", "56-78", "-9 end");
        assertParseDecimalsFromStringNormal(testingList, 123456789);

        testingList = List.of("The most", " popular", " tank of WW2", " is T-34-85");
        assertParseDecimalsFromStringNormal(testingList, 23485);

    }


    private void assertParseDecimalsFromStringNormal(List<String> strings, long expected) {
        long actual = Parse.parseDecimalsFromString(strings);
        assertEquals(expected, actual);
    }

    private void assertParseDecimalsFromStringException(List<String> strings, Exception expected) {
        try {
            Parse.parseDecimalsFromString(strings);
        } catch (Exception e) {
            assertEquals(expected.getMessage(), e.getMessage());
            assertEquals(expected.getClass(), e.getClass());
        }
    }
}