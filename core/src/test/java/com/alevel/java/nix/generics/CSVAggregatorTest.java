package com.alevel.java.nix.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVAggregatorTest {

    @Test
    void aggregate() {
        assertCSVStrings(new String[]{"One", "Two", "Three"}, "One,Two,Three");
        assertCSVStrings(new String[]{}, null);
        assertCSVStrings(null, null);
        assertCSVIntegers(new Integer[]{1, 2, 3, 4, 5}, "1,2,3,4,5");
        assertCSVIntegers(new Integer[]{}, null);
        assertCSVIntegers(null, null);
    }

    private void assertCSVStrings(String[] arr, String expected) {
        CSVAggregator<String> csvAggregator = new CSVAggregator<>();
        String actual = csvAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

    private void assertCSVIntegers(Integer[] arr, String expected) {
        CSVAggregator<Integer> csvAggregator = new CSVAggregator<>();
        String actual = csvAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }
}