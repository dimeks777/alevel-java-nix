package com.alevel.java.nix.hometask.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {
    private BubbleSort bubbleSort;

    @BeforeEach
    void setUp() {
        bubbleSort = new BubbleSort();
    }

    @Test
    void testBubbleSort() {
        assertBubbleSortInt(new int[]{6, 9, 3, 2, 5, 1, 7}, new int[]{1, 2, 3, 5, 6, 7, 9});
        assertBubbleSortInt(new int[]{-1, -10, -4, 0, 10, -8}, new int[]{-10, -8, -4, -1, 0, 10});
        assertBubbleSortInt(new int[]{726, 425, 711, 372, 911}, new int[]{372, 425, 711, 726, 911});
        assertBubbleSortDouble(new double[]{15.1, 27.32, 9.18, 23.1, 30.96}, new double[]{9.18, 15.1, 23.1, 27.32, 30.96});
        assertBubbleSortIntReverse(new int[]{6, 9, 3, 2, 5, 1, 7}, new int[]{9, 7, 6, 5, 3, 2, 1});
        assertBubbleSortIntReverse(new int[]{-1, -10, -4, 0, 10, -8}, new int[]{10, 0, -1, -4, -8, -10});
        assertBubbleSortIntReverse(new int[]{726, 425, 711, 372, 911}, new int[]{911, 726, 711, 425, 372});
        assertBubbleSortDoubleReverse(new double[]{15.1, 27.32, 9.18, 23.1, 30.96}, new double[]{30.96, 27.32, 23.1, 15.1, 9.18});
    }

    private void assertBubbleSortInt(int[] arr, int[] sortedArr) {
        int[] actual = bubbleSort.bubbleSortInt(arr);
        assertArrayEquals(sortedArr, actual);
    }

    private void assertBubbleSortDouble(double[] arr, double[] sortedArr) {
        double[] actual = bubbleSort.bubbleSortDouble(arr);
        assertArrayEquals(sortedArr, actual);
    }

    private void assertBubbleSortIntReverse(int[] arr, int[] sortedArr) {
        int[] actual = bubbleSort.bubbleSortIntReverse(arr);
        assertArrayEquals(sortedArr, actual);
    }

    private void assertBubbleSortDoubleReverse(double[] arr, double[] sortedArr) {
        double[] actual = bubbleSort.bubbleSortDoubleReverse(arr);
        assertArrayEquals(sortedArr, actual);
    }

}