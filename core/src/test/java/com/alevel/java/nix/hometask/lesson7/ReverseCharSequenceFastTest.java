package com.alevel.java.nix.hometask.lesson7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseCharSequenceFastTest {

    @Test
    void testReverseCharSequenceFast() {
        assertReverse("abc", new ReverseCharSequenceFast("cba"));
        assertReverse(null, null);
        assertReverse("a", new ReverseCharSequenceFast("a"));
        assertReverse("", new ReverseCharSequenceFast(""));
    }

    private void assertReverse(CharSequence base, ReverseCharSequenceFast expected) {
        ReverseCharSequenceFast actual = ReverseCharSequenceFast.reverse(base);
        if (actual == null) {
            assertNull(expected);
        } else {
            assertEquals(expected.toString(), actual.toString());
        }
    }

}