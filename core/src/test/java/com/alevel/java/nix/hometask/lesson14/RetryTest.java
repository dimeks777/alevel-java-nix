package com.alevel.java.nix.hometask.lesson14;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {


    @Test
    void testRetry() {
        assertExceptionCalculateInteger(new CalculateBlock<>(1, 0), new ArithmeticException("/ by zero"), 5);
        assertExceptionCalculateInteger(new CalculateBlock<>(1, null), new NullPointerException(), 3);
        assertNormalCalculateDouble(new CalculateBlock<>(5.0, -1.0), 5, 5.0);
        assertNormalCalculateDouble(new CalculateBlock<>(0.0, 1.0), 1, 1.0);
        assertExceptionReader(new ReadingBlock<>("notes3.txt", new CalculateBlock<>(1, 2)),
                new FileNotFoundException("notes3.txt (Не удается найти указанный файл)"), 2);
        assertExceptionReader(new ReadingBlock<>(null, new CalculateBlock<>(-1, 20)),
                new NullPointerException(), 1);
        assertNormalReaderDouble(new ReadingBlock<>("log\\1.txt", 2.0), 10, 2.0);
        assertNormalReaderDouble(new ReadingBlock<>("log\\1.txt", -134.0), 5, -134.0);
    }

    private void assertNormalCalculateDouble(CalculateBlock<Double> doubleCalculateBlock, int attempts, Double expected) {
        Retry<Double> retry = new Retry<>(100, doubleCalculateBlock, attempts);
        try {
            Double actual = retry.run();
            assertEquals(expected, actual);
        } catch (Exception e) {
            assertNull(e);
        }
    }

    private void assertExceptionCalculateInteger(CalculateBlock<Integer> block, Exception expected, int countOfAttempts) {
        Retry<Integer> retry = new Retry<>(100, block, countOfAttempts);
        try {
            retry.run();
        } catch (Exception e) {
            assertEquals(expected.getMessage(), e.getMessage());
        }
    }

    private void assertExceptionReader(ReadingBlock<CalculateBlock<Integer>> block, Exception expected, int countOfAttempts) {
        Retry<CalculateBlock<Integer>> retry = new Retry<>(100, block, countOfAttempts);
        try {
            retry.run();
        } catch (Exception e) {
            assertEquals(expected.getMessage(), e.getMessage());
        }
    }

    private void assertNormalReaderDouble(ReadingBlock<Double> doubleReadingBlock, int attempts, Double expected) {
        Retry<Double> retry = new Retry<>(100, doubleReadingBlock, attempts);
        try {
            Double actual = retry.run();
            assertEquals(expected, actual);
        } catch (Exception e) {
            assertNull(e);
        }
    }

}