package com.alevel.java.nix.iopractice.assignment1;

import com.alevel.java.nix.hometask.lesson15.ForwardLinkedList;
import com.alevel.java.nix.hometask.lesson15.ForwardLinkedList;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

import static org.junit.jupiter.api.Assertions.*;

class FolderCopyTest {

    private static final Logger logger = LoggerFactory.getLogger(FolderCopyTest.class);
    private File folder1 = new File("C:\\Users\\dimek\\alevel-java-nix\\core\\log");
    private File folder2 = new File("C:\\Users\\dimek\\alevel-java-nix\\log");
    private ForwardLinkedList<File> listFiles = new ForwardLinkedList<>();

    @Test
    void copyFolder() {


        assertCopyFolder(folder1, folder2, true);

        listFiles.add(createFile(folder1, "123.txt"));
        assertCopyFolder(folder1, folder2, true);
        listFiles.add(createFile(folder2, "123.txt"));


        listFiles.add(createFile(folder2, "1234.txt"));
        assertCopyFolder(folder1, folder2, false);
        cleanFiles();
    }

    private File createFile(File folder, String name) {
        File newFile = null;
        if (name != null) {
            try {
                newFile = new File(folder, name);
                logger.info("A new file was created: {} {}", newFile.createNewFile(), newFile.getAbsolutePath());
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
        return newFile;
    }

    private void cleanFiles() {
        if (listFiles.size() != 0) {
            for (File f : listFiles) {
                String fName = f.getAbsolutePath();
                if (f.delete()) {
                    logger.info("File deleted: {}", fName);
                }
            }
        }
    }

    private void assertCopyFolder(File src, File dest, boolean expected) {
        FolderCopy.copyFolder(src, dest);
        assertEquals(expected, new FolderComparator().areDirsEqual(src, dest));
    }

}