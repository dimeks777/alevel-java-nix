package com.alevel.java.nix.hometask.lesson14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class ReadingBlock<T> implements Block<T> {
    private static final Logger logger = LoggerFactory.getLogger(ReadingBlock.class);
    private String fileName;
    private T data;

    public ReadingBlock(String fileName, T data) {
        this.fileName = fileName;
        this.data = data;
    }


    @Override
    public T run() throws Exception {
        try {
            FileReader in = new FileReader(fileName);
            FileWriter out = new FileWriter(fileName);
            out.write(data.getClass().toString());
            logger.info("Writing successful.");
            int i;
            StringBuilder stringBuilder = new StringBuilder();
            out.close();
            while ((i = in.read()) != -1) {
                stringBuilder.append((char) i);
            }
            in.close();
            System.out.println(stringBuilder.toString());
            String temp = stringBuilder.toString();
            return data.getClass().toString().equals(stringBuilder.toString()) ? data : null;
        } catch (Exception e) {
            logger.error(String.valueOf(new Exception(e.getMessage())));
            throw new Exception(e.getMessage());
        }
    }

}
