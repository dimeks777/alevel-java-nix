package com.alevel.java.nix.hometask.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxProfitFinderTest {
    private MaxProfitFinder maxProfitFinder;

    @BeforeEach
    void setUp() {
        maxProfitFinder = new MaxProfitFinder();
    }

    @Test
    void testGetMaxProfit() {
        assertProfit(null, 0, 0, 0);
        assertProfit(new double[]{9.2, 12.1, 1.5, 2.7, 4.3, 10.3, 3.9}, 2, 5, 8.8);
        assertProfit(new double[]{10.7, 5.4, 7.1, 11.6, 2.8, 6.6, 9.3}, 4, 6, 6.5);
        assertProfit(new double[]{0, 0, 0, 0, 0, 0, 0}, 6, 6, 0);
        assertProfit(new double[]{-5.1, -7.7, -12.8, -2.2, 0, 5.1, 4.6}, 2, 5, 17.9);

    }

    private void assertProfit(double[] stocksArr, int expectedBuyDay, int expectedSellDay, double expectedProfit) {
        maxProfitFinder = MaxProfitFinder.getMaxProfit(stocksArr);
        if (maxProfitFinder == null) {
            assertNull(stocksArr);
        } else {
            assertEquals(expectedBuyDay, maxProfitFinder.getBuyDay());
            assertEquals(expectedSellDay, maxProfitFinder.getSellDay());
            assertEquals(expectedProfit, maxProfitFinder.getProfit());
        }


    }


}