package com.alevel.java.nix.premodule.task1;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WeekendFinderTest {

    @Test
    public void getListOfWeekends() {
        assertGetListOfWeekends("2020-04-03", "2020-04-06", List.of(LocalDate.parse("2020-04-04"), LocalDate.parse("2020-04-05")), null);
        assertGetListOfWeekends(null, null, null, new NullPointerException());
    }

    private void assertGetListOfWeekends(CharSequence date1, CharSequence date2, List<LocalDate> expected, Exception exp) {
        try {
            WeekendFinder weekendFinder = new WeekendFinder(date1, date2);
            assertEquals(expected, weekendFinder.getListOfWeekends());
        } catch (Exception e) {
            assertEquals(exp.getClass(), e.getClass());
        }
    }
}