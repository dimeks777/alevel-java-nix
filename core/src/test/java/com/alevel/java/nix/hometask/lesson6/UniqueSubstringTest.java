package com.alevel.java.nix.hometask.lesson6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueSubstringTest {

    @Test
    void testgetLongestUniqueSubstringLength() {
        assertUniqueSubstringLength(null, 0);
        assertUniqueSubstringLength("abcabcbb", 3);
        assertUniqueSubstringLength("bbbbb", 1);
        assertUniqueSubstringLength("pwwkew", 3);
    }

    private void assertUniqueSubstringLength(String base, int expected) {
        int actual = UniqueSubstring.getLongestUniqueSubstringLength(base);
        assertEquals(expected, actual);
    }


}