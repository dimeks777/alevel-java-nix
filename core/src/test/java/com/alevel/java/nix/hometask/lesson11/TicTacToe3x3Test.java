package com.alevel.java.nix.hometask.lesson11;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToe3x3Test {

    @Test
    void testSetSymbol() {
        assertSetSymbol(new char[][]{
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, new char[][]{
                {'X', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, 1, 'X');

        assertSetSymbol(new char[][]{
                {'X', 'O', 'O'},
                {' ', 'X', 'X'},
                {' ', ' ', 'O'}
        }, new char[][]{
                {'X', 'O', 'O'},
                {' ', 'X', 'X'},
                {'O', ' ', 'O'}
        }, 2, 'O');
    }


    @Test
    void testWon() {
        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'O', 'X', 'X'},
                {' ', ' ', 'X'}
        }, 'X', 1, 1, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'O', 'X', 'O'},
                {' ', ' ', 'O'}
        }, 'O', 2, 2, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'X', 'O', 'O'},
                {'O', ' ', 'X'}
        }, 'O', 2, 0, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'X', 'X', 'O'},
                {'O', 'O', 'X'}
        }, 'O', 2, 0, false);
    }

    @Test
    void testIsDraw() {
        assertDraw(new char[][]{
                {'X', 'O', 'X'},
                {'X', 'X', 'O'},
                {'O', 'X', 'O'}
        }, true);
        assertDraw(new char[][]{
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, false);

    }

    @Test
    void testCheckGameStatus() {
        assertGameStatus(new char[][]{
                {' ', 'O', 'X'},
                {'X', 'X', 'O'},
                {'O', 'X', ' '}
        }, TicTacToe3x3.PLAYER2, 1, 1, TicTacToe3x3.INGAME);
        assertGameStatus(new char[][]{
                {'X', 'O', 'X'},
                {'O', 'X', 'O'},
                {'O', 'X', 'X'},
        }, TicTacToe3x3.PLAYER1, 2, 2, TicTacToe3x3.PLAYER1WON);

    }

    private void assertGameStatus(char[][] grid, int currentPlayer, int lastSetSymbolRow, int lastSetSymbolCol, int expected) {
        TicTacToe3x3 game = new TicTacToe3x3(TicTacToe3x3.INGAME, currentPlayer, lastSetSymbolRow, lastSetSymbolCol, grid);
        game.checkGameStatus(lastSetSymbolRow, lastSetSymbolCol);
        assertEquals(expected, game.getCurrentGameStatus());

    }

    private void assertSetSymbol(char[][] grid, char[][] expected, int position, char symbol) {
        TicTacToe3x3 game = new TicTacToe3x3(grid);
        game.updateGrid(symbol, position);
        assertArrayEquals(expected, game.getGrid());

    }

    private void assertWon(char[][] grid, char symbol, int lastSetSymbolRow, int lastSetSymbolCol, boolean expected) {
        TicTacToe3x3 game = new TicTacToe3x3(grid, lastSetSymbolRow, lastSetSymbolCol);
        boolean actual = game.hasWon(symbol, lastSetSymbolRow, lastSetSymbolCol);
        assertEquals(expected, actual);
    }

    private void assertDraw(char[][] grid, boolean expected) {
        TicTacToe3x3 game = new TicTacToe3x3(grid);
        boolean actual = game.isDraw();
        assertEquals(expected, actual);
    }


}