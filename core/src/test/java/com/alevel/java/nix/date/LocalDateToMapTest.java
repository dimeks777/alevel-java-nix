package com.alevel.java.nix.date;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class LocalDateToMapTest {

    @Test
    void localDateToMapConverter() {
        LocalDateTime now = LocalDateTime.of(2020, 3, 23, 12, 0);
        List<LocalDateTime> data = Arrays.asList(
                now,
                now.minusHours(1),
                now.plusHours(1),
                now.minusDays(1),
                now.plusDays(1)
        );


        Map<LocalDate, SortedSet<LocalTime>> expected = new HashMap<>() {
            {
                put(now.minusDays(1).toLocalDate(), new TreeSet<>() {
                    {
                        add(now.minusDays(1).toLocalTime());
                    }

                });
                put(now.toLocalDate(), new TreeSet<>() {
                    {
                        add(now.minusHours(1).toLocalTime());
                        add(now.toLocalTime());
                        add(now.plusHours(1).toLocalTime());
                    }

                });
                put(now.plusDays(1).toLocalDate(), new TreeSet<>() {
                    {
                        add(now.plusDays(1).toLocalTime());
                    }
                });
            }
        };
        assertEquals(expected, LocalDateToMap.localDateToMapConverter(data));

    }
}