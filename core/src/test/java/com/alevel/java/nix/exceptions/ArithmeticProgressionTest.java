package com.alevel.java.nix.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArithmeticProgressionTest {

    @Test
    void testCalculateArithmeticProgression() throws ProgressionConfigurationException {
        assertCalculateArithmeticProgression(1, 1, 3, 4);
        assertCalculateArithmeticProgression(5, 5, 2, 15);
        assertCalculateArithmeticProgression(0, 1, 2, 2);

        assertCalculateArithmeticProgressionException(1, 0, 1, ProgressionConfigurationException.stepException);
        assertCalculateArithmeticProgressionException(0, 0, 0, ProgressionConfigurationException.stepException);
        assertCalculateArithmeticProgressionException(10, 5, -1, ProgressionConfigurationException.indexException);

    }

    private void assertCalculateArithmeticProgression(int initial, int step, int index, int expected) throws ProgressionConfigurationException {
        ArithmeticProgression arithmeticProgression = new ArithmeticProgression(initial, step);
        assertEquals(expected, arithmeticProgression.calculate(index));
    }

    private void assertCalculateArithmeticProgressionException(int initial, int step, int index, String expectedMessage) {
        assertThrows(ProgressionConfigurationException.class, () -> new ArithmeticProgression(initial, step).calculate(index), expectedMessage);
    }
}