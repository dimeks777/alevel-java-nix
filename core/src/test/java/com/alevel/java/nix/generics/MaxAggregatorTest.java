package com.alevel.java.nix.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxAggregatorTest {

    @Test
    void aggregate() {
        assertMaxInteger(new Integer[]{-1, 10, 0, -5, 100}, 100);
        assertMaxInteger(new Integer[]{}, null);
        assertMaxInteger(null, null);
        assertMaxDouble(new Double[]{3.0, 2.0, 7.0, -1.0}, 7.0);
        assertMaxDouble(new Double[]{}, null);
        assertMaxDouble(null, null);
    }

    private void assertMaxDouble(Double[] arr, Double expected) {
        MaxAggregator<Double> maxAggregator = new MaxAggregator<>();
        Double actual = maxAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

    private void assertMaxInteger(Integer[] arr, Integer expected) {
        MaxAggregator<Integer> maxAggregator = new MaxAggregator<>();
        Integer actual = maxAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

}