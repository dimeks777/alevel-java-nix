package com.alevel.java.nix.hometask.lesson14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalculateBlock<T extends Number> implements Block<T> {

    private static final Logger logger = LoggerFactory.getLogger(CalculateBlock.class);

    private T first;
    private T second;

    public CalculateBlock(T first, T second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public T run() throws Exception {
        try {
            int result = first.intValue() / second.intValue();
            logger.info("Calculating successful (first = {}, second = {})", first, second);
            return Math.abs(result) > 1 ? first : second;
        } catch (Exception e) {
            logger.error("Invalid operation:", new Exception(e.getMessage()));
            throw new Exception(e.getMessage());
        }
    }

}
