package com.alevel.java.nix.hometask.lesson12.task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {

    @Test
    void testFindContractStudents() {
        assertContractStudents(new Student[]{new Student("Dmitry", 19), new ContractStudent("Andrii", 23, 22000),
                new ContractStudent("Maxim", 22, 13000),
                new Student("Sergey", 22)}, new Student[]{new ContractStudent("Andrii", 23, 22000),
                new ContractStudent("Maxim", 22, 13000)});
        assertContractStudents(null, null);
    }

    private void assertContractStudents(Student[] students, Student[] expected) {
        Group group = new Group(students);
        Student[] actual = group.findContractStudents();
        if (actual == null) {
            assertNull(expected);
        } else {
            assertEquals(expected.length, actual.length);
            for (int i = 0; i < actual.length; i++) {
                assertEquals(expected[i].getName(), group.findContractStudents()[i].getName());
                assertEquals(expected[i].getAge(), group.findContractStudents()[i].getAge());
            }
        }
    }

}
