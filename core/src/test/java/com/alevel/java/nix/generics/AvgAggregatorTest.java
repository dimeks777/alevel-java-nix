package com.alevel.java.nix.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvgAggregatorTest {

    @Test
    void aggregate() {
        assertAvgDouble(new Double[]{1.0, 2.0, 3.0, 4.0, 5.0}, 3.0);
        assertAvgDouble(null, null);
        assertAvgInteger(new Integer[]{1, 2}, 1.5);
        assertAvgInteger(null, null);
    }

    private void assertAvgDouble(Double[] arr, Double expected) {
        AvgAggregator<Double> avgAggregator = new AvgAggregator<>();
        Double actual = avgAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }

    private void assertAvgInteger(Integer[] arr, Double expected) {
        AvgAggregator<Integer> avgAggregator = new AvgAggregator<>();
        Double actual = avgAggregator.aggregate(arr);
        if (actual != null) {
            assertEquals(expected, actual);
        } else {
            assertNull(expected);
        }
    }
}