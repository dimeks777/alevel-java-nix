package com.alevel.java.nix.hometask.lesson9;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    @Test
    void testSetSymbol() {
        assertSetSymbol(new char[][]{
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, new char[][]{
                {'X', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, 1, 'X');

        assertSetSymbol(new char[][]{
                {'X', 'O', 'O'},
                {' ', 'X', 'X'},
                {' ', ' ', 'O'}
        }, new char[][]{
                {'X', 'O', 'O'},
                {' ', 'X', 'X'},
                {'O', ' ', 'O'}
        }, 2, 'O');
    }


    @Test
    void testWon() {
        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'O', 'X', 'X'},
                {' ', ' ', 'X'}
        }, 'X', 1, 1, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'O', 'X', 'O'},
                {' ', ' ', 'O'}
        }, 'O', 2, 2, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'X', 'O', 'O'},
                {'O', ' ', 'X'}
        }, 'O', 2, 0, true);

        assertWon(new char[][]{
                {'X', 'O', 'O'},
                {'X', 'X', 'O'},
                {'O', 'O', 'X'}
        }, 'O', 2, 0, false);
    }

    @Test
    void testIsDraw() {
        assertDraw(new char[][]{
                {'X', 'O', 'X'},
                {'X', 'X', 'O'},
                {'O', 'X', 'O'}
        }, true);
        assertDraw(new char[][]{
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        }, false);

    }

    private void assertSetSymbol(char[][] grid, char[][] expected, int position, char symbol) {
        TicTacToe game = new TicTacToe(grid);
        TicTacToe.setSymbol(symbol, position);
        assertArrayEquals(expected, game.getGrid());

    }

    private void assertWon(char[][] grid, char symbol, int lastSetSymbolRow, int lastSetSymbolCol, boolean expected) {
        new TicTacToe(grid, lastSetSymbolRow, lastSetSymbolCol);
        boolean actual = TicTacToe.hasWon(symbol, lastSetSymbolRow, lastSetSymbolCol);
        assertEquals(expected, actual);
    }

    private void assertDraw(char[][] grid, boolean expected) {
        new TicTacToe(grid);
        boolean actual = TicTacToe.isDraw();
        assertEquals(expected, actual);
    }

}