package com.alevel.java.nix.hometask.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultipleNumbersTest {

    private MultipleNumbers multipleNumbers;

    @BeforeEach
    void setUp() {
        multipleNumbers = new MultipleNumbers();
    }

    @Test
    void testMultipleNumbers() {
        assertMultipleInt(new int[]{6, 9, 3, 2, 5, 1, 7}, 3, new int[]{6, 9, 3});
        assertMultipleInt(new int[]{-1, -10, -4, 0, 10, -8}, 0, null);
        assertMultipleInt(new int[]{726, 425, 711, 372, 911}, 1, new int[]{726, 425, 711, 372, 911});
        assertMultipleDouble(new double[]{15.1, 27.32, 9.18, 23.1, 30.96}, 9.18, new double[]{9.18});
    }

    private void assertMultipleInt(int[] arr, int k, int[] arrOfMultiple) {
        int[] actual = multipleNumbers.multipleNumbersInt(arr, k);
        if (actual == null && arrOfMultiple == null) {
            assertEquals(0, 0);
        } else {
            assertArrayEquals(arrOfMultiple, actual);
        }

    }

    private void assertMultipleDouble(double[] arr, double k, double[] arrOfMultiple) {
        double[] actual = multipleNumbers.multipleNumbersDouble(arr, k);
        if (actual == null && arrOfMultiple == null) {
            assertEquals(0, 0);
        } else {
            assertArrayEquals(arrOfMultiple, actual);
        }

    }


}