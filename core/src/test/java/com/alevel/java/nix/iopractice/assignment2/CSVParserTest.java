package com.alevel.java.nix.iopractice.assignment2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CSVParserTest {

    private CSVTable csvTable = new CSVTable(CSVParser.parse("C:\\Users\\dimek\\alevel-java-nix\\core\\log\\table.csv"));

    @Test
    void parse() {
        assertParseByTwoIndexes(0, 3, null);
        assertParseByTwoIndexes(-1, 3, null);
        assertParseByTwoIndexes(0, 15, null);
        assertParseByTwoIndexes(15, -999, null);
        assertParseByTwoIndexes(3, 3, "2001");
        assertParseByIndexAndHeader(0, "Name", "Peter");
        assertParseByIndexAndHeader(3, "Name", "Mark");
        assertParseByIndexAndHeader(15, "Surname", null);
        assertParseByIndexAndHeader(-15, "Date of birth", null);
        assertParseByIndexAndHeader(2, "Surname", "Kulyk");

    }

    private void assertParseByTwoIndexes(int row, int col, String expected) {
        assertEquals(expected, csvTable.accessValue(row, col));
    }

    private void assertParseByIndexAndHeader(int row, String header, String expected) {
        assertEquals(expected, csvTable.accessValue(row, header));
    }
}