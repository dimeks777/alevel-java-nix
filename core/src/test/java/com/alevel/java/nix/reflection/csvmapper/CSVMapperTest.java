package com.alevel.java.nix.reflection.csvmapper;

import com.alevel.java.nix.iopractice.assignment2.CSVParser;
import com.alevel.java.nix.iopractice.assignment2.CSVTable;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVMapperTest {

    @Test
    void parse() throws Exception {
        List<Person> expected = List.of(new Person("Mike", 27, "male", "janitor", 25500.0, true),
                new Person("Beth", 23, "female", "recruiter", 12600.0, false));
        String source = Paths.get(CSVMapper.class.getResource("/personaldata.csv").toURI()).toString();
        assertParse(source, expected);
    }

    private void assertParse(String path, List<Person> expected) throws Exception {
        List<Person> actual = CSVMapper.parse(new CSVTable(CSVParser.parse(path)), Person.class);
        for (int i = 0; i < actual.size(); i++) {
            assertEquals(expected.get(i), actual.get(i));
        }
    }
}