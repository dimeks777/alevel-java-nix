package com.alevel.java.nix.hometask.lesson10;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestCommonPrefixTest {


    @Test
    void testGetLongestCommonPrefix() {
        assertLongestCommonPrefix(new String[]{
                "flower",
                "flow",
                "flight",
        }, "fl");
        assertLongestCommonPrefix(new String[]{
                "dog",
                "racecar",
                "car",
        }, "");
        assertLongestCommonPrefix(new String[]{
                "dog",
                "racecar",
                "car",
        }, "");
        assertLongestCommonPrefix(new String[]{
                "tank",
                "",
                "fighter",
        }, "");
        assertLongestCommonPrefix(null, null);
        assertLongestCommonPrefix(new String[]{
                "cloud",
        },"cloud");
    }

    private void assertLongestCommonPrefix(String[] strings, String expected) {
        String actual = LongestCommonPrefix.getLongestCommonPrefix(strings);
        if (actual == null) {
            assertNull(expected);
        } else {
            assertEquals(expected, actual);
        }
    }


}