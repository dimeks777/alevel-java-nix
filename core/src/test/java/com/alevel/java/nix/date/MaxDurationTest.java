package com.alevel.java.nix.date;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MaxDurationTest {

    @Test
    void findMaxDuration() {
        List<LocalDateTime> localDateTimes = Arrays.asList(
                LocalDateTime.of(2020, 3, 23, 15, 0),
                LocalDateTime.of(1966, 11, 21, 12, 12),
                LocalDateTime.of(1967, 11, 21, 12, 12),
                LocalDateTime.of(1968, 11, 21, 12, 12)
        );

        Duration maxDurationCycle = MaxDuration.findMaxDurationCycle(localDateTimes);
        assertEquals(19481, maxDurationCycle.toDays());
        Duration maxDurationStream = MaxDuration.findMaxDurationStream(localDateTimes);
        assertEquals(19481, maxDurationStream.toDays());

    }
}