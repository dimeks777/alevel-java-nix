package com.alevel.java.nix.hometask.lesson6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxAreaTest {

    @Test
    void testGetMaxArea() {
        assertMaxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}, 49);
        assertMaxArea(new int[]{}, 0);
        assertMaxArea(null, 0);
        assertMaxArea(new int[]{-1, -8, -6, -2, -5, -4, -8, -3, -7}, 49);
    }

    private void assertMaxArea(int[] values, int expected) {
        int actual = MaxArea.getMaxArea(values);
        assertEquals(expected, actual);
    }

    @Test
    void testGetMaxAreaFast() {
        assertMaxAreaFast(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}, 49);
        assertMaxAreaFast(new int[]{}, 0);
        assertMaxAreaFast(null, 0);
        assertMaxAreaFast(new int[]{-1, -8, -6, -2, -5, -4, -8, -3, -7}, 49);
    }

    private void assertMaxAreaFast(int[] values, int expected) {
        int actual = MaxArea.getMaxAreaFast(values);
        assertEquals(expected, actual);
    }
}