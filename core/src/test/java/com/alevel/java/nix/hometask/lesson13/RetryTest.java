package com.alevel.java.nix.hometask.lesson13;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {

    @Test
    void testRepeat() {
        assertException(new CalculateBlock(0, 1), 5, new ArithmeticException("/ by zero"));
        assertException(new ReadingBlock("notes3.txt"), 3, new FileNotFoundException("notes3.txt (Не удается найти указанный файл)"));
        assertNormal(new CalculateBlock(1, 5), 10);
        assertNormal(new ReadingBlock("log\\tic-tac-toe.txt"), 5);
    }

    private void assertException(Block block, int n, Exception expected) {
        Retry retry = new Retry(100);
        try {
            retry.repeat(block, n);
        } catch (Exception e) {
            assertEquals(expected.getMessage(), e.getMessage());
        }
    }

    private void assertNormal(Block block, int n) {
        Retry retry = new Retry(100);
        try {
            retry.repeat(block, n);
        } catch (Exception e) {
            assertNull(e);
        }
    }


}