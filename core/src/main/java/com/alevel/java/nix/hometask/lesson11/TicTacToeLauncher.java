package com.alevel.java.nix.hometask.lesson11;

public class TicTacToeLauncher {
    public static void main(String[] args) {
        var gameLoop = new TicTacToeGameLoop3x3(new TicTacToe3x3());
        gameLoop.playGame();
    }

}
