package com.alevel.java.nix.premodule.task2.model;

public interface Hangman {

    String getAdjustedWord();

    String getCurrentWordStatus();

    String result(char in);

    boolean hasSymbol(char in);

    String decode(char in);

    String encode(String word);
}
