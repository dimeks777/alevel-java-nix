package com.alevel.java.nix.hometask.lesson14;

@FunctionalInterface
public interface Block<T> {
    T run() throws Exception;
}
