package com.alevel.java.nix.hometask.lesson13;

public class Retry {

    private final int basicDelay;

    public Retry(int basicDelay) {
        this.basicDelay = basicDelay;
    }

    void repeat(Block block, int n) throws Exception {
        Exception err = null;
        for (int i = 0; i < n; i++) {
            try {
                block.run();
                return;
            } catch (Exception e) {
                err = e;
                Thread.sleep(basicDelay * (i + 1));
            }
        }
        if (err != null) throw err;
    }

}
