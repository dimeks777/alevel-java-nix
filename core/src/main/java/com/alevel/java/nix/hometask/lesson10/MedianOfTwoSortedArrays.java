package com.alevel.java.nix.hometask.lesson10;

public class MedianOfTwoSortedArrays {

    private static double calculateMedian(int[] arr) {
        int center = arr.length / 2;
        if (arr.length % 2 != 0) {
            return arr[center];
        }
        return (arr[center - 1] + arr[center]) / 2.0;
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        if (nums1 == null && nums2 == null) {
            return 0;
        } else if (nums1 == null || nums2 == null) {
            return (nums1 == null) ? calculateMedian(nums2) : calculateMedian(nums1);
        }
        if (nums1.length > nums2.length) {
            return findMedianSortedArrays(nums2, nums1);
        }
        int x = nums1.length;
        int y = nums2.length;
        int start = 0;
        int end = x;

        while (start <= end) {
            int partX = start + (end - start) / 2;
            int partY = (x + y + 1) / 2 - partX;

            int maxLeftX = partX == 0 ? Integer.MIN_VALUE : nums1[partX - 1];
            int maxLeftY = partY == 0 ? Integer.MIN_VALUE : nums2[partY - 1];
            int minRightX = partX == x ? Integer.MAX_VALUE : nums1[partX];
            int minRightY = partY == y ? Integer.MAX_VALUE : nums2[partY];

            if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
                if ((x + y) % 2 == 0) {
                    return (double) (Math.max(maxLeftX, maxLeftY) + Math.min(minRightX, minRightY)) / 2;
                } else {
                    return Math.max(maxLeftX, maxLeftY);
                }
            } else if (maxLeftX > minRightY) {
                end = partX - 1;
            } else {
                start = partX + 1;
            }
        }
        return 0;
    }

}
