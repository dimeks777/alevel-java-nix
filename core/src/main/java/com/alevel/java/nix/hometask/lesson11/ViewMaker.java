package com.alevel.java.nix.hometask.lesson11;

public interface ViewMaker {

    char[][] convertForPrinting();

}
