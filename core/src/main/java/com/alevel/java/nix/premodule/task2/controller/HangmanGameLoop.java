package com.alevel.java.nix.premodule.task2.controller;

import com.alevel.java.nix.premodule.task2.model.HangmanStates;
import com.alevel.java.nix.premodule.task2.model.Hangman;
import com.alevel.java.nix.premodule.task2.view.View;
import com.alevel.java.nix.premodule.task2.model.HangmanStates;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class HangmanGameLoop {

    private InputStream input;
    private PrintStream output;
    private Hangman model;

    public HangmanGameLoop(InputStream input, PrintStream output, Hangman model) {
        this.input = input;
        this.output = output;
        this.model = model;
    }

    public void loop() {
        if (model.getAdjustedWord() != null && model.getCurrentWordStatus() != null) {
            output.println("Game started");
            View view = new View(HangmanStates.NO_ERRORS, model.getAdjustedWord(), model.getCurrentWordStatus());
            boolean win = false;
            boolean lose;
            var scanner = new Scanner(input);
            do {
                view.print(output);
                output.println("Player, enter your character:");
                char ch = scanner.next().charAt(0);
                view.updateView(model.result(ch), model.getCurrentWordStatus());
            } while (!((lose = view.getState().equals(HangmanStates.SIX_ERRORS)) || model.getCurrentWordStatus().equals(model.getAdjustedWord())));

            if (lose) {
                view.printLose(output);
            } else {
                view.printWin(output);
            }
        }

    }
}
