package com.alevel.java.nix.hometask.lesson6;

public class MaxArea {
    public static int getMaxArea(int[] height) {
        if (height == null || height.length == 0) return 0;
        int maxVolume = 0;
        int currentVolume = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = 1; j < height.length; j++) {
                currentVolume = Math.min(Math.abs(height[i]), Math.abs(height[j])) * (j - i);
                maxVolume = Math.max(currentVolume, maxVolume);
            }

        }
        return maxVolume;
    }

    public static int getMaxAreaFast(int[] height) {
        if (height == null || height.length == 0) return 0;
        int maxVolume = 0;
        int currentVolume;
        int start = 0;
        int end = height.length - 1;
        while (start < end) {
            currentVolume = Math.min(Math.abs(height[start]), Math.abs(height[end])) * (end - start);
            maxVolume = Math.max(currentVolume, maxVolume);
            if (Math.abs(height[start]) < Math.abs(height[end])) {
                start++;
            } else {
                end--;
            }
        }
        return maxVolume;
    }

}
