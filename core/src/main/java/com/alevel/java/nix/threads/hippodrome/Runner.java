package com.alevel.java.nix.threads.hippodrome;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    private static final int COUNT_OF_LANES = 5;

    public static void main(String[] args) throws InterruptedException {

        int bet = getBet();

        List<String> resultList = Collections.synchronizedList(new ArrayList<>());
        CountDownLatch readyThreadCounter = new CountDownLatch(5);
        CountDownLatch callingThreadBlocker = new CountDownLatch(1);
        CountDownLatch completedThreadCounter = new CountDownLatch(5);
        List<Thread> horses = Stream
                .generate(() -> new Thread(new Horse(
                        resultList, readyThreadCounter, callingThreadBlocker, completedThreadCounter)))
                .limit(COUNT_OF_LANES)
                .collect(Collectors.toList());

        int i = 1;
        for (Thread horse : horses) {
            horse.setName("Horse" + i++);
        }

        horses.forEach(Thread::start);
        readyThreadCounter.await();
        callingThreadBlocker.countDown();
        completedThreadCounter.await();
        String winner = resultList.get(0);

        System.out.println("The winner is: " + winner);
        if (bet == Integer.parseInt(winner.replaceAll("[\\D]", ""))) {
            System.out.println("Bet wins !");
        } else {
            System.out.println("The bet is lost");
        }
    }


    public static int getBet() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your bet:");
        int bet = 0;
        boolean acceptable = false;
        while (!acceptable) {
            try {
                bet = scanner.nextInt();
                acceptable = true;
            } catch (InputMismatchException e) {
                System.out.println("Just enter a number ");
            }
        }
        return bet;
    }

}
