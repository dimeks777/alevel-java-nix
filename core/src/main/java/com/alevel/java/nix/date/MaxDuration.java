package com.alevel.java.nix.date;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class MaxDuration {

    public static Duration findMaxDurationCycle(Collection<LocalDateTime> list) {
        if (list != null) {
            Iterator<LocalDateTime> iterator = list.iterator();
            LocalDateTime before = iterator.next();
            LocalDateTime current = iterator.next();
            Duration maxDuration = Duration.between(before, current);
            Duration currentDuration;
            while (iterator.hasNext()) {
                before = current;
                current = iterator.next();
                currentDuration = Duration.between(before, current).abs();
                maxDuration = maxDuration.abs().toMillis() > currentDuration.abs().toMillis() ? maxDuration.abs() : currentDuration.abs();
            }
            return maxDuration;
        }
        throw new NullPointerException();

    }

    public static Duration findMaxDurationStream(Collection<LocalDateTime> list) {
        LocalDateTime min = list.stream().sorted().findFirst().orElse(null);
        LocalDateTime max = list.stream().sorted().skip(list.size() - 1).findFirst().orElse(null);
        if (min != null && max != null) return Duration.between(min, max);
        throw new NullPointerException();
    }

}
