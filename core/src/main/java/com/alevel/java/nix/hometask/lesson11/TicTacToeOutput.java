package com.alevel.java.nix.hometask.lesson11;

public interface TicTacToeOutput {

    void printEmptyIndexes();

    void printGame();
}
