package com.alevel.java.nix.hometask.lesson12.task2;

public class Iron implements Substance {

    private double temperature;
    private static final double FREEZING = 1538.0;
    private static final double BOILING = 2862.0;
    private State state;

    Iron() {
        this.temperature = defaultTemperature;
    }

    @Override
    public State heatUp(double t) {
        this.temperature += t;
        if (temperature <= FREEZING) {
            state = State.HARD;
        } else if (temperature > FREEZING && temperature < BOILING) {
            state = State.LIQUID;
        } else if (temperature >= BOILING) {
            state = State.GASEOUS;
        }
        return state;
    }

    @Override
    public double getTemperature() {
        return temperature;
    }
}
