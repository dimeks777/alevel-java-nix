package com.alevel.java.nix.generics;

public class AvgAggregator<T extends Number> implements Aggregator<Double, T> {
    @Override
    public Double aggregate(T[] items) {
        if (items != null) {
            if (items.length == 0) return 0.0;
            if (items.length == 1) return items[0].doubleValue();
            double sum = 0.0;
            for (T elem : items) {
                sum += elem.doubleValue();
            }
            return sum / items.length;
        }
        return null;
    }
}
