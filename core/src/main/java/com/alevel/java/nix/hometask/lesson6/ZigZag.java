package com.alevel.java.nix.hometask.lesson6;

public class ZigZag {

    public static String convert(String s, int numRows) {
        if(s == null) return null;
        if (numRows == 1 ||s.length() == 0) return s;
        int countOfMiddleRows = numRows - 2;
        StringBuilder sb = new StringBuilder();
        int step = numRows + countOfMiddleRows;
        char[] chars = s.toCharArray();
        for (int row = 0; row < numRows; row++) {
            int index = row;
            while (index < chars.length) {
                sb.append(chars[index]);
                if (row == 0 || row == numRows - 1) {
                    index += step;
                } else {
                    index += (numRows + countOfMiddleRows - row * 2);
                    if (index < chars.length) {
                        sb.append(chars[index]);
                    }
                    index += 2 * row;
                }
            }
        }

        return sb.toString();
    }

}
