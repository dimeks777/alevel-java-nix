package com.alevel.java.nix.hometask.lesson12.task1;

import java.util.ArrayList;

public class Group {

    private Student[] students;

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    Group(Student[] students) {
        this.students = students;
    }

    public Student[] findContractStudents() {
        if (this.students == null) return null;
        int countOfContractStudents = 0;
        ArrayList<Student> listOfContractStudents = new ArrayList<Student>();
        for (Student student : students) {
            if (student instanceof ContractStudent) {
                listOfContractStudents.add(student);
                countOfContractStudents++;
            }
        }
        Student[] arr = new Student[countOfContractStudents];
        for (int i = 0; i < countOfContractStudents; i++) {
            arr[i] = listOfContractStudents.get(i);
        }
        return arr;
    }

    public static void printStudents(Student[] students) {
        for (Student student : students) {
            System.out.println("Name: " + student.getName() + " Contract cost: " + ((ContractStudent) student).getContractCost());
        }
    }

}
