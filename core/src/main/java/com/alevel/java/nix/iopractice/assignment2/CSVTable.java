package com.alevel.java.nix.iopractice.assignment2;

import java.util.HashMap;
import java.util.List;

public class CSVTable {
    private List<String> headers;
    private List<String> values;
    private HashMap<String, Integer> headerIndexes;

    public List<String> getHeaders() {
        return headers;
    }

    public List<String> getValues() {
        return values;
    }

    public HashMap<String, Integer> getHeaderIndexes() {
        return headerIndexes;
    }


    public CSVTable(List<String> headers, List<String> values, HashMap<String, Integer> headerIndexes) {
        this.headers = headers;
        this.values = values;
        this.headerIndexes = headerIndexes;
    }

    public CSVTable(CSVTable csvTable) {
        this.headers = csvTable.headers;
        this.values = csvTable.values;
        this.headerIndexes = csvTable.headerIndexes;
    }

    public String accessValue(int row, int col) {
        if (row >= 0 && col >= 0 && !(col >= headers.size() || row >= values.size() / col)) {
            return values.get(row * headers.size() + col);
        }
        return null;
    }

    public String accessValue(int row, String header) {
        int index = headerIndexes.get(header);
        if (index != -1 && !(row >= values.size() / headers.size()) && row >= 0) {
            return values.get(row * headers.size() + index);
        }
        return null;
    }

}
