package com.alevel.java.nix.hometask.lesson10;

public class LongestCommonPrefix {

    public static String getLongestCommonPrefix(String[] strs) {
        if (strs == null) return null;
        if (strs.length == 1) return strs[0];
        boolean isEquals = true;
        int minLength = Integer.MAX_VALUE;
        int temp = 0;
        int savedIndex = 0;
        for (int i = 0; i < strs.length; i++) {
            temp = Math.min(minLength, strs[i].length());
            if (temp < minLength) {
                savedIndex = i;
                minLength = temp;
            }
        }
        if (minLength == 0) return "";
        StringBuilder sb = new StringBuilder();
        char symbol;
        for (int i = 0; (i < minLength) && isEquals; i++) {
            symbol = strs[savedIndex].charAt(i);
            for (String str : strs) {
                if (str.charAt(i) != symbol) {
                    isEquals = false;
                    break;
                }
            }
            if (isEquals) {
                sb.append(symbol);
            }
        }
        return sb.toString();
    }

}
