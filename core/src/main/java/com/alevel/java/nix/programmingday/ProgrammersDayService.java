package com.alevel.java.nix.programmingday;

import java.time.LocalDate;

class ProgrammersDayService {
    static final int PROGRAMMERS_DAY = 256;
    private final DateWizard dateWizard;

    ProgrammersDayService(DateWizard dateWizard) {
        this.dateWizard = dateWizard;
    }

    LocalDate getProgrammingDay(int year) {
        return dateWizard.getDateOfYear(year, PROGRAMMERS_DAY);
    }
}
