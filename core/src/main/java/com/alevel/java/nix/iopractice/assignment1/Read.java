package com.alevel.java.nix.iopractice.assignment1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collection;

public class Read {

    public static Collection<String> readLinesHaveSubString(String filename, CharSequence substring) {
        Collection<String> lines;
        if (filename != null) {
            try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
                String line;
                lines = new ArrayList<>();
                while ((line = reader.readLine()) != null) {
                    if (line.contains(substring)) lines.add(line);
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
            return lines;
        }
        return null;
    }

    public static void linesHaveSubstringPrinter(Collection<String> list) {
        for (String s : list) {
            System.out.println(s);
        }
    }

    public static void readAndPrintLinesHaveSubstring(String filename,CharSequence substring){
        Collection<String> strings = readLinesHaveSubString(filename,substring);
        linesHaveSubstringPrinter(strings);
    }
}
