package com.alevel.java.nix.premodule.task1;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class WeekendFinder {

    private LocalDate date1;
    private LocalDate date2;

    public WeekendFinder(CharSequence date1, CharSequence date2) {
        if (date1 != null && date2 != null) {
            this.date1 = formattedLocalDate(date1);
            this.date2 = formattedLocalDate(date2);
        } else {
            throw new NullPointerException();
        }
    }

    private LocalDate formattedLocalDate(CharSequence date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public List<LocalDate> getListOfWeekends() {

        List<LocalDate> weekends = new ArrayList<>();
        do {
            DayOfWeek dayOfWeek = date1.getDayOfWeek();
            if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY) {
                if (dayOfWeek == DayOfWeek.SATURDAY) {
                    weekends.add(date1);
                    weekends.add(date1.plusDays(1));
                    date1 = date1.plusDays(7);
                } else {
                    weekends.add(date1);
                    date1 = date1.plusDays(6);
                }

            } else {
                date1 = date1.plusDays(1);
            }

        } while (!date1.isAfter(date2));

        return weekends;
    }

    public static void printList(List<LocalDate> list) {
        for (LocalDate localDate : list) {
            System.out.println(localDate);
        }
    }

}
