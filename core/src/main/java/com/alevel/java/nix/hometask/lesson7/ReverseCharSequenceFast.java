package com.alevel.java.nix.hometask.lesson7;

//Original: https://stackoverflow.com/questions/3109914/reverse-a-string-in-java-in-o1

public final class ReverseCharSequenceFast implements CharSequence {
    private final CharSequence original;

    public ReverseCharSequenceFast(CharSequence original) {
        this.original = original;
    }

    public int length() {
        return original.length();
    }

    public char charAt(int index) {
        return original.charAt(original.length() - index - 1);
    }

    public CharSequence subSequence(int start, int end) {
        int originalEnd = original.length() - start;
        int originalStart = original.length() - end;
        return new ReverseCharSequenceFast(
                original.subSequence(originalStart, originalEnd));
    }

    public String toString() {
        return new StringBuilder(this).toString();
    }

    public static ReverseCharSequenceFast reverse(CharSequence original) {
        if (original == null) return null;
        return new ReverseCharSequenceFast(new ReverseCharSequenceFast(original).toString());
    }


}