package com.alevel.java.nix.hometask.lesson3;

class MultipleNumbers {

    int[] multipleNumbersInt(int[] arr, int k) {
        if (k != 0) {
            int[] temp = new int[arr.length];
            int counter = 0;
            for (int value : arr) {
                if (value % k == 0) {
                    temp[counter++] = value;
                }
            }
            int[] newArr = new int[counter];
            System.arraycopy(temp, 0, newArr, 0, counter);
            return newArr;

        } else {
            System.out.println("ERROR!The number 0 is not a multiple of any number!");
            return null;
        }

    }

    double[] multipleNumbersDouble(double[] arr, double k) {
        if (k != 0.0) {
            double[] temp = new double[arr.length];
            int counter = 0;
            for (double value : arr) {
                if (value % k == 0.0) {
                    temp[counter++] = value;
                }
            }
            double[] newArr = new double[counter];
            System.arraycopy(temp, 0, newArr, 0, counter);
            return newArr;

        } else {
            System.out.println("ERROR!The number 0 is not a multiple of any number!");
            return null;
        }

    }

}
