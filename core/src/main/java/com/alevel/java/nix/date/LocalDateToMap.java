package com.alevel.java.nix.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.util.stream.Collectors.*;


public class LocalDateToMap {

    public static Map<LocalDate, SortedSet<LocalTime>> localDateToMapConverter(Collection<LocalDateTime> list) {
        return list.stream().collect(groupingBy(
                LocalDateTime::toLocalDate,
                TreeMap::new,
                mapping(LocalDateTime::toLocalTime, toCollection(TreeSet::new))
        ));
    }

}
