package com.alevel.java.nix.hometask.lesson12.task2;

public interface Substance {

    double defaultTemperature = 20;

    State heatUp(double t);

    double getTemperature();
}
