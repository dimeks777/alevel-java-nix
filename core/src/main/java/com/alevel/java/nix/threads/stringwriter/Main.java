package com.alevel.java.nix.threads.stringwriter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class Main {
    private static String input = "initial";
    private static Path path = null;
    private static StringBuilder stringBuilder = new StringBuilder("initial");

    public static void main(String[] args) {
        try {
            path = Paths.get(Main.class.getResource("/output.txt").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        boolean init = true;

        if (path != null) {
            Scanner scanner = new Scanner(System.in);
            WriterThread writer = new WriterThread(input);
            Thread child = new Thread(writer);
            child.start();
            while (true) {
                if (child.isAlive()) {
                    if (!input.equals("quit")) {
                        input = scanner.nextLine();
                        if (init) {
                            stringBuilder.setLength(0);
                            stringBuilder = new StringBuilder(input).append("\n");
                            init = false;
                        } else {
                            stringBuilder.append(input).append("\n");
                        }

                    }
                } else {
                    System.out.println("Finishing...");
                    return;
                }
            }
        }

    }

    private static class WriterThread implements Runnable {

        private static String temp;


        public WriterThread(String s) {
            temp = s;
        }

        @Override
        public void run() {
            System.out.println("Writer thread started");
            write();
            System.out.println("Writer thread ended");
        }

        public static void write() {

            while (true) {
                String newStr = stringBuilder.toString();
                if (!newStr.equals(temp)) {
                    if (!input.toLowerCase().equals("quit")) {
                        try {
                            Files.write(path, newStr.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
                            System.out.println("String was written:" + input);
                            temp = newStr;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        return;
                    }
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
