package com.alevel.java.nix.premodule.task2.model;

import java.util.HashMap;

public class HangmanStates {
    public static final String NO_ERRORS =    " ____\n |  |\n    |\n    |\n    |\n    |";
    public static final String ONE_ERROR =    " ____\n |  |\n O  |\n    |\n    |\n    |";
    public static final String TWO_ERRORS =   " ____\n |  |\n O  |\n |  |\n    |\n    |";
    public static final String THREE_ERRORS = " ____\n |  |\n O  |\n/|  |\n    |\n    |";
    public static final String FOUR_ERRORS =  " ____\n |  |\n O  |\n/|\\ |\n    |\n    |";
    public static final String FIVE_ERRORS =  " ____\n |  |\n O  |\n/|\\ |\n/   |\n    |";
    public static final String SIX_ERRORS =   " ____\n |  |\n O  |\n/|\\ |\n/ \\ |\n    |";;

    private static final HashMap<Integer,String> states = new HashMap<>(7){
        {
            put(0,NO_ERRORS);
            put(1,ONE_ERROR);
            put(2,TWO_ERRORS);
            put(3,THREE_ERRORS);
            put(4,FOUR_ERRORS);
            put(5,FIVE_ERRORS);
            put(6,SIX_ERRORS);
        }
    };

    public static String getState(int countOfErrors){
        return states.get(countOfErrors);
    }
}
