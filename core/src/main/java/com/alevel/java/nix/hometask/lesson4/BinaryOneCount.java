package com.alevel.java.nix.hometask.lesson4;

public class BinaryOneCount {
    public int count(long number) {
        long temp = 0;
        int binaryOneCount = 0;
        while (number != 0) {
            temp = number;
            if ((temp & 0b1) != 0) {
                binaryOneCount++;
            }
            number >>>= 1;
        }
        return binaryOneCount;
    }


}
