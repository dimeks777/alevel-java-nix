package com.alevel.java.nix.hometask.lesson11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class TicTacToeIO3x3 implements ViewMaker, UserInput, TicTacToeOutput {

    private TicTacToe3x3 model;
    private static final Logger logger = LoggerFactory.getLogger("com.alevel.java.nix.hometask.lesson11.TicTacToeIO3x3");

    public TicTacToeIO3x3(TicTacToe3x3 model) {
        this.model = model;
    }

    public int inputPosition() {
        Scanner in = new Scanner(System.in);
        printEmptyIndexes();
        boolean validate = false;
        int pos = 0;
        while (!validate) {
            pos = in.nextInt();
            if (pos > model.getCountOfEmptyPlaces() || pos < 1) {
                System.out.println("Wrong place! Try again.");
                logger.warn("Player{} puts invalid position", model.getCurrentPlayer());
            } else {
                validate = true;
            }
        }

        model.setCountOfEmptyPlaces(model.getCountOfEmptyPlaces() - 1);
        return pos;
    }

    public void printGame() {
        char[][] temp = convertForPrinting();
        for (char[] row : temp) {
            for (char col : row) {
                System.out.print(col);
            }
            System.out.println();
        }
    }

    public void printEmptyIndexes() {
        char[][] temp = convertForPrinting();
        int emptyIndexPos = '1';
        for (char[] chars : temp) {
            for (int j = 0; j < temp[0].length; j++) {
                if (chars[j] == ' ') {
                    System.out.print((char) emptyIndexPos++);
                } else {
                    System.out.print(chars[j]);
                }
            }
            System.out.println();
        }
    }

    @Override
    public char[][] convertForPrinting() {
        char[][] gameBoard = {
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '}
        };
        for (int i = 0, row = 0; i < gameBoard.length; i += 2, row++) {
            for (int j = 0, col = 0; j < gameBoard[0].length; j += 2, col++) {
                gameBoard[i][j] = model.getGrid()[row][col];
            }
        }
        return gameBoard;
    }
}
