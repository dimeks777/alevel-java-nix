package com.alevel.java.nix.hometask.lesson5;

public class MatrixTranspose {


    public int[][] transposeMatrix(int[][] matrix) {
        if (matrix != null) {
            int rows = matrix.length;
            int cols = matrix[0].length;
            int[][] newMatrix = new int[cols][rows];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    newMatrix[j][i] = matrix[i][j];
                }
            }
            return newMatrix;
        } else {
            return null;
        }
    }

}
