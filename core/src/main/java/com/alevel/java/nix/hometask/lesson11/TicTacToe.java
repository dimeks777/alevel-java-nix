package com.alevel.java.nix.hometask.lesson11;

public interface TicTacToe {

    void initGame();

    boolean hasWon(int symbol, int currentRow, int currentCol);

    void updateGrid(char symbol, int position);

    void playerMove();

    void checkGameStatus(int currentRow, int currentCol);

    boolean isDraw();

}
