package com.alevel.java.nix.iopractice.assignment1;

import java.io.File;

public class FolderComparator {
    private boolean result = true;

    public boolean areDirsEqual(File dir1, File dir2) {
        try {
            if (dir1.isDirectory() && dir2.isDirectory()) {
                File[] files1 = dir1.listFiles();
                File[] files2 = dir2.listFiles();

                if (files1 != null && files2 != null) {
                    String[] fNames1 = dir1.list();
                    String[] fNames2 = dir2.list();
                    if (fNames1 != null && fNames2 != null) {
                        if (fNames1.length == fNames2.length) {
                            for (int i = 0; i < fNames1.length; i++) {
                                if (!result) return false;
                                result = fNames1[i].equals(fNames2[i]);
                                File srcFile = new File(dir1, fNames1[i]);
                                File destFile = new File(dir2, fNames2[i]);
                                areDirsEqual(srcFile, destFile);
                            }
                        } else {
                            result = false;
                            return false;
                        }
                    }
                }
            } else {
                return dir1.getName().equals(dir2.getName());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return true;
    }
}
