package com.alevel.java.nix.exceptions;

public class ArithmeticProgression {

    private final int initial;
    private final int step;

    ArithmeticProgression(int initial, int step) throws ProgressionConfigurationException {
        this.initial = initial;
        this.step = step;
    }


    int calculate(int n) throws ProgressionConfigurationException {

        if (n <= 0) {
            throw new ProgressionConfigurationException(ProgressionConfigurationException.indexException);
        } else if (step == 0) {
            throw new ProgressionConfigurationException(ProgressionConfigurationException.stepException);
        }
        int result = initial;
        for (int i = 0; i < n; i++) {
            result += step;
        }
        return result;
    }

}
