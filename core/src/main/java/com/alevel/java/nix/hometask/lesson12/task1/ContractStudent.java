package com.alevel.java.nix.hometask.lesson12.task1;

public class ContractStudent extends Student {
    private double contractCost;

    public double getContractCost() {
        return contractCost;
    }

    public void setContractCost(double contractCost) {
        this.contractCost = contractCost;
    }

    ContractStudent(String name,int age,double contractCost){
        super(name,age);
        this.contractCost = contractCost;

    }

}
