package com.alevel.java.nix.hometask.lesson15;

import java.util.List;

public class Parse {
    public static final String errorIllegalArgumentsMessage = "No digits found in ";
    public static final String errorNullPointerMessage = "The value of parsing object is null";

    public static long parseDecimalsFromString(List<String> strings) {
        if (strings != null) {
            return strings.stream()
                    .flatMapToInt(String::codePoints)
                    .filter(Character::isDigit)
                    .map(codepoint -> Character.digit(codepoint, 10))
                    .asLongStream()
                    .reduce((result, next) -> result * 10 + next)
                    .orElseThrow(() -> new IllegalArgumentException(errorIllegalArgumentsMessage + strings));
        }
        throw new NullPointerException(errorNullPointerMessage);
    }

}
