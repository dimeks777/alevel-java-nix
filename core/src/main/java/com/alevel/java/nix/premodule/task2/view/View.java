package com.alevel.java.nix.premodule.task2.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.util.HashSet;

public class View {
    private String state;
    private final String definedWord;
    private String currentUserWord;
    private int totalMoves = 0;
    private HashSet<Character> uniqueCharacters = new HashSet<>();
    private static final Logger logger = LoggerFactory.getLogger(View.class);


    public View(String state, String definedWord, String currentUserWord) {
        this.state = state;
        this.definedWord = definedWord;
        this.currentUserWord = currentUserWord;
    }

    public String getCurrentUserWord() {
        return currentUserWord;
    }

    public String getState() {
        return state;
    }

    public void updateView(String state, String currentUserWord) {
        this.state = state;
        this.currentUserWord = currentUserWord;
    }

    public void cls() {
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
    }

    public void print(PrintStream target) {
        cls();
        target.println(state);
        target.println("Word: " + currentUserWord);
        totalMoves++;
    }

    public void printLose(PrintStream target) {
        cls();
        target.println("GAME OVER!");
        target.println(state);
        target.println("Adjusted word:" + definedWord);
        logger.info("Player loses the game");
    }

    public void printWin(PrintStream target) {
        cls();
        target.println("YOU WON!");
        target.println(state);
        target.println("Adjusted word:" + definedWord);
        target.println("Total moves: " + totalMoves + " - " + rank());
        logger.info("Player wins the game");
    }

    private void countUniqueCharacters() {
        for (int i = 1, length = definedWord.length() - 1; i < length; i++) {
            uniqueCharacters.add(definedWord.charAt(i));
        }
    }

    private String rank() {
        countUniqueCharacters();
        int size = uniqueCharacters.size();
        if (totalMoves == size) {
            return "PERFECT";
        } else if (totalMoves == size + 1) {
            return "GOOD";
        } else if (totalMoves == size + 2) {
            return "NOT BAD";
        } else if (totalMoves > size + 2) {
            return "TRY BETTER";
        }
        return null;
    }
}
