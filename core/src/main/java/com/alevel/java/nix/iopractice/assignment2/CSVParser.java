package com.alevel.java.nix.iopractice.assignment2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toMap;

public class CSVParser {

    public static CSVTable parse(String filename) {
        List<String> headers = new ArrayList<>();
        List<String> values = new ArrayList<>();
        HashMap<String, Integer> headerIndexes = new HashMap<>();
        boolean headersAreRead = false;
        if (filename != null) {
            try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!headersAreRead) {
                        headers.addAll(Arrays.asList(line.split(",")));
                        headerIndexes = (HashMap<String, Integer>) IntStream.range(0, headers.size())
                                .boxed()
                                .collect(toMap(headers::get, Function.identity()));
                        headersAreRead = true;
                    } else {
                        if (headers.size() > 0) {
                            String[] temp = new String[headers.size()];
                            String[] actualValues = line.split(",");
                            System.arraycopy(actualValues, 0, temp, 0, actualValues.length);
                            values.addAll(Arrays.asList(temp));
                        }
                    }
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
            return new CSVTable(headers, values, headerIndexes);
        }
        return null;
    }

}
