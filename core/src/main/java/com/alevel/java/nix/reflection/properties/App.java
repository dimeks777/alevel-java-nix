package com.alevel.java.nix.reflection.properties;


public class App {
    public static void main(String[] args) {
        AppProperties appProperties = new AppProperties();
        try {
            new Initialize().initValues(appProperties, AppProperties.class);
            System.out.println(appProperties);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
