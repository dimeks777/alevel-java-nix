package com.alevel.java.nix.iopractice.assignment1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FolderCopy {
    private static final Logger logger = LoggerFactory.getLogger(FolderCopy.class);

    public static void copyFolder(File sourceFolder, File destinationFolder) {
        try {
            if (sourceFolder.isDirectory()) {
                if (!destinationFolder.exists()) {
                    if (destinationFolder.mkdir()) {
                        logger.info("Directory created: {}", destinationFolder);
                    }
                }
                String[] files = sourceFolder.list();
                if (files != null) {

                    for (String file : files) {
                        File srcFile = new File(sourceFolder, file);
                        File destFile = new File(destinationFolder, file);
                        copyFolder(srcFile, destFile);
                    }
                }
            } else {
                Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
                logger.info("File copied: {}", destinationFolder);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}

