package com.alevel.java.nix.hometask.lesson9;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TicTacToe {

    private static final int PLAYER1 = 1;
    private static final int PLAYER2 = 2;
    private static final int ROWS = 3;
    private static final int COLS = 3;
    private static final char EMPTY = ' ';
    private static final char CROSS = 'X';
    private static final char NOUGHT = 'O';
    private static final int INGAME = 111;
    private static final int PLAYER1WON = 666;
    private static final int PLAYER2WON = 777;
    private static final int DRAW = 999;

    private static int currentGameStatus;
    private static int currentPlayer;
    private static int countOfEmptyPlaces = 9;
    private static int lastSetSymbolRow;
    private static int lastSetSymbolCol;

    private static char[][] grid = new char[ROWS][COLS];
    private static final Logger logger = LoggerFactory.getLogger("com.alevel.java.nix.hometask.lesson9.TicTacToe");

    TicTacToe(char[][] grid, int lastSetSymbolRow, int lastSetSymbolCol) {
        TicTacToe.grid = grid;
        TicTacToe.lastSetSymbolRow = lastSetSymbolRow;
        TicTacToe.lastSetSymbolCol = lastSetSymbolCol;
    }


    TicTacToe(char[][] arr) {
        grid = arr;
    }

    char[][] getGrid() {
        return grid;
    }

    public static void initGame() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                grid[row][col] = EMPTY;
            }
        }
        currentGameStatus = INGAME;
        currentPlayer = PLAYER1;
        logger.info("Grid initialized and ready for game");
    }

    public static void printGame() {
        char[][] temp = convertForPrinting();
        for (char[] row : temp) {
            for (char col : row) {
                System.out.print(col);
            }
            System.out.println();
        }

    }

    public static boolean hasWon(int symbol, int currentRow, int currentCol) {
        return (grid[currentRow][0] == symbol
                && grid[currentRow][1] == symbol
                && grid[currentRow][2] == symbol
                || grid[0][currentCol] == symbol
                && grid[1][currentCol] == symbol
                && grid[2][currentCol] == symbol
                || currentRow == currentCol
                && grid[0][0] == symbol
                && grid[1][1] == symbol
                && grid[2][2] == symbol
                || currentRow + currentCol == 2
                && grid[0][2] == symbol
                && grid[1][1] == symbol
                && grid[2][0] == symbol);
    }

    public static char[][] convertForPrinting() {
        char[][] gameBoard = {
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '}
        };
        for (int i = 0, row = 0; i < gameBoard.length; i += 2, row++) {
            for (int j = 0, col = 0; j < gameBoard[0].length; j += 2, col++) {
                gameBoard[i][j] = grid[row][col];
            }
        }
        return gameBoard;
    }

    public static void main(String[] args) {
        playGame();
    }

    public static void printEmptyIndexes() {
        char[][] temp = TicTacToe.convertForPrinting();
        int emptyIndexPos = '1';
        for (char[] chars : temp) {
            for (int j = 0; j < temp[0].length; j++) {
                if (chars[j] == ' ') {
                    System.out.print((char) emptyIndexPos++);
                } else {
                    System.out.print(chars[j]);
                }
            }
            System.out.println();
        }
    }

    public static int inputPosition() {
        Scanner in = new Scanner(System.in);
        printEmptyIndexes();
        boolean validate = false;
        int pos = 0;
        while (!validate) {
            pos = in.nextInt();
            if (pos > countOfEmptyPlaces || pos < 1) {
                System.out.println("Wrong place! Try again.");
                logger.warn("Player{} puts invalid position",currentPlayer);
            } else {
                validate = true;
            }
        }

        countOfEmptyPlaces--;
        System.out.println(countOfEmptyPlaces);
        return pos;
    }

    public static void setSymbol(char symbol, int position) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == EMPTY && count < position) {
                    count++;
                    if (count == position) {
                        grid[i][j] = symbol;
                        lastSetSymbolRow = i;
                        lastSetSymbolCol = j;

                        logger.info("Player{} puts a character on position [{},{}]", currentPlayer, i, j);
                        return;
                    }
                }
            }
        }
    }

    public static void playerMove(int currentPlayer) {
        if (currentPlayer == PLAYER1) {
            System.out.println("Move of PLAYER1");
        } else {
            System.out.println("Move of PLAYER2");
        }
        setSymbol((currentPlayer == PLAYER1) ? CROSS : NOUGHT, inputPosition());

    }

    public static void checkGameStatus(int currentPlayer, int currentRow, int currentCol) {
        if (hasWon((currentPlayer == PLAYER1) ? CROSS : NOUGHT, currentRow, currentCol)) {
            currentGameStatus = (currentPlayer == PLAYER1) ? PLAYER1WON : PLAYER2WON;
        } else if (isDraw()) {
            currentGameStatus = DRAW;
        }
    }


    public static boolean isDraw() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                if (grid[row][col] == EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }


    public static void playGame() {
        logger.info("Game starts");
        initGame();
        do {
            playerMove(currentPlayer);
            checkGameStatus(currentPlayer, lastSetSymbolRow, lastSetSymbolCol);
            printGame();
            if (currentGameStatus == PLAYER1WON) {
                System.out.println("Player1(X) won!");
                logger.info("Player1 won");
            } else if (currentGameStatus == PLAYER2WON) {
                System.out.println("Player2(O) won!");
                logger.info("Player2 won");
            } else if (currentGameStatus == DRAW) {
                System.out.println("Draw!");
                logger.info("Draw");
            }

            currentPlayer = (currentPlayer == PLAYER1) ? PLAYER2 : PLAYER1;
        } while (currentGameStatus == INGAME);
    }


}
