package com.alevel.java.nix.premodule.task2.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassicHangman implements Hangman {

    private final String adjustedWord;
    private String currentWordStatus;
    private int errors = 0;
    private static final Logger logger = LoggerFactory.getLogger(ClassicHangman.class);

    public ClassicHangman(String path) {
        this.adjustedWord = Input.chooseWord(path);
        if (adjustedWord != null) {
            this.currentWordStatus = encode(adjustedWord);
        } else logger.error("Error while starting the game,closing...");
        logger.info("Game initialized and ready. Starting...");
    }

    public ClassicHangman(String adjustedWord, String currentWordStatus, int errors) {
        this.adjustedWord = adjustedWord;
        this.currentWordStatus = currentWordStatus;
        this.errors = errors;
    }

    @Override
    public String getAdjustedWord() {
        return adjustedWord;
    }

    public String getCurrentWordStatus() {
        return currentWordStatus;
    }

    public int getErrors() {
        return errors;
    }

    @Override
    public String encode(String word) {
        char[] arr = word.toCharArray();
        for (int i = 1, length = word.length() - 1; i < length; i++) {
            arr[i] = '*';
        }
        return new String(arr);
    }


    @Override
    public String result(char in) {
        if (hasSymbol(in)) {
            currentWordStatus = decode(in);
        } else {
            errors++;
        }
        return HangmanStates.getState(errors);
    }

    @Override
    public boolean hasSymbol(char in) {
        for (int i = 0, length = adjustedWord.length(); i < length; i++) {
            if (Character.toLowerCase(in) == Character.toLowerCase(adjustedWord.charAt(i))) return true;
        }
        logger.info("Player inserts incorrect letter: {}. Tries left: {}", in, 6 - errors);
        return false;
    }

    @Override
    public String decode(char in) {
        in = Character.toLowerCase(in);
        char[] arr = currentWordStatus.toCharArray();
        boolean alreadyOpened = false;
        for (int i = 1, length = adjustedWord.length() - 1; i < length; i++) {
            if (in == Character.toLowerCase(adjustedWord.charAt(i))) {
                if (arr[i] == in) {
                    alreadyOpened = true;
                } else arr[i] = in;
            }
        }
        if (alreadyOpened) errors++;
        logger.info("Player opens a letter: {}", in);
        return new String(arr);
    }
}
