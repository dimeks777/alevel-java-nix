package com.alevel.java.nix.hometask.lesson12.task2;

public class Oxygen implements Substance {

    private double temperature;
    private static final double FREEZING = -220.0;
    private static final double BOILING = -183.0;
    private State state;

    Oxygen() {
        this.temperature = defaultTemperature;
    }

    @Override
    public State heatUp(double t) {
        this.temperature += t;
        if (temperature <= FREEZING) {
            state = State.HARD;
        } else if (temperature > FREEZING && temperature < BOILING) {
            state = State.LIQUID;
        } else if (temperature >= BOILING) {
            state = State.GASEOUS;
        }
        return state;
    }

    @Override
    public double getTemperature() {
        return temperature;
    }

}
