package com.alevel.java.nix.hometask.lesson11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TicTacToe3x3 implements TicTacToe {

    static final int PLAYER1 = 1;
    static final int PLAYER2 = 2;
    static final int ROWS = 3;
    static final int COLS = 3;
    static final char EMPTY = ' ';
    static final char CROSS = 'X';
    static final char NOUGHT = 'O';
    static final int INGAME = 111;
    static final int PLAYER1WON = 666;
    static final int PLAYER2WON = 777;
    static final int DRAW = 999;


    private int currentGameStatus;
    private int currentPlayer;
    private int countOfEmptyPlaces = 9;
    private int lastSetSymbolRow;
    private int lastSetSymbolCol;

    private char[][] grid = new char[ROWS][COLS];
    private final TicTacToeIO3x3 io = new TicTacToeIO3x3(this);


    private static final Logger logger = LoggerFactory.getLogger("com.alevel.java.nix.hometask.lesson11.TicTacToe3x3");


    public TicTacToe3x3(int currentGameStatus, int currentPlayer, int lastSetSymbolRow, int lastSetSymbolCol, char[][] grid) {
        this.currentGameStatus = currentGameStatus;
        this.currentPlayer = currentPlayer;
        this.lastSetSymbolRow = lastSetSymbolRow;
        this.lastSetSymbolCol = lastSetSymbolCol;
        this.grid = grid;
    }


    public TicTacToe3x3() {

    }

    public TicTacToe3x3(char[][] grid) {
        this.grid = grid;
    }

    public TicTacToe3x3(char[][] grid, int lastSetSymbolRow, int lastSetSymbolCol) {
        this.grid = grid;
    }

    @Override
    public void initGame() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                grid[row][col] = EMPTY;
            }
        }
        currentGameStatus = INGAME;
        currentPlayer = PLAYER1;
        logger.info("Grid initialized and ready for game");
    }

    @Override
    public boolean hasWon(int symbol, int currentRow, int currentCol) {
        return (grid[currentRow][0] == symbol
                && grid[currentRow][1] == symbol
                && grid[currentRow][2] == symbol
                || grid[0][currentCol] == symbol
                && grid[1][currentCol] == symbol
                && grid[2][currentCol] == symbol
                || currentRow == currentCol
                && grid[0][0] == symbol
                && grid[1][1] == symbol
                && grid[2][2] == symbol
                || currentRow + currentCol == 2
                && grid[0][2] == symbol
                && grid[1][1] == symbol
                && grid[2][0] == symbol);
    }

    @Override
    public void updateGrid(char symbol, int position) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == EMPTY && count < position) {
                    count++;
                    if (count == position) {
                        grid[i][j] = symbol;
                        lastSetSymbolRow = i;
                        lastSetSymbolCol = j;

                        logger.info("Player{} puts a character on position [{},{}]", currentPlayer, i, j);
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void playerMove() {
        if (currentPlayer == PLAYER1) {
            System.out.println("Move of PLAYER1");
        } else {
            System.out.println("Move of PLAYER2");
        }
        updateGrid((currentPlayer == PLAYER1) ? CROSS : NOUGHT, io.inputPosition());
    }

    @Override
    public void checkGameStatus(int currentRow, int currentCol) {
        if (hasWon((currentPlayer == PLAYER1) ? CROSS : NOUGHT, currentRow, currentCol)) {
            currentGameStatus = (currentPlayer == PLAYER1) ? PLAYER1WON : PLAYER2WON;
        } else if (isDraw()) {
            currentGameStatus = DRAW;
        }
    }

    @Override
    public boolean isDraw() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                if (grid[row][col] == EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public TicTacToeIO3x3 getIo() {
        return this.io;
    }

    public int getCurrentGameStatus() {
        return currentGameStatus;
    }

    public void setCurrentGameStatus(int currentGameStatus) {
        this.currentGameStatus = currentGameStatus;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public int getCountOfEmptyPlaces() {
        return countOfEmptyPlaces;
    }

    public void setCountOfEmptyPlaces(int countOfEmptyPlaces) {
        this.countOfEmptyPlaces = countOfEmptyPlaces;
    }

    public int getLastSetSymbolRow() {
        return lastSetSymbolRow;
    }

    public void setLastSetSymbolRow(int lastSetSymbolRow) {
        this.lastSetSymbolRow = lastSetSymbolRow;
    }

    public int getLastSetSymbolCol() {
        return lastSetSymbolCol;
    }

    public void setLastSetSymbolCol(int lastSetSymbolCol) {
        this.lastSetSymbolCol = lastSetSymbolCol;
    }

    public char[][] getGrid() {
        return grid;
    }

    public void setGrid(char[][] grid) {
        this.grid = grid;
    }
}
