package com.alevel.java.nix.premodule.task2;

import com.alevel.java.nix.premodule.task2.controller.HangmanGameLoop;
import com.alevel.java.nix.premodule.task2.model.ClassicHangman;
import com.alevel.java.nix.premodule.task2.controller.HangmanGameLoop;
import com.alevel.java.nix.premodule.task2.model.ClassicHangman;

public class HangmanRunner {
    public static void main(String[] args) {
        var gameLoop = new HangmanGameLoop(System.in, System.out, new ClassicHangman("/hangman.txt"));
        gameLoop.loop();
    }
}
