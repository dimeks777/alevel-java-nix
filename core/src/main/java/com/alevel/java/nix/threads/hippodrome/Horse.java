package com.alevel.java.nix.threads.hippodrome;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

public class Horse implements Runnable {
    private final List<String> resultList;
    private final CountDownLatch readyThreadCounter;
    private final CountDownLatch callingThreadBlocker;
    private final CountDownLatch completedThreadCounter;
    private static final Integer RACE_DISTANCE = 1000;
    private static final Integer MIN_DISTANCE_PER_SNATCH = 100;
    private static final Integer MAX_DISTANCE_PER_SNATCH = 200;
    private static final Integer MIN_REFRESH_TIME = 400;
    private static final Integer MAX_REFRESH_TIME = 500;

    public Horse(
            List<String> resultList,
            CountDownLatch readyThreadCounter,
            CountDownLatch callingThreadBlocker,
            CountDownLatch completedThreadCounter) {

        this.resultList = resultList;
        this.readyThreadCounter = readyThreadCounter;
        this.callingThreadBlocker = callingThreadBlocker;
        this.completedThreadCounter = completedThreadCounter;
    }

    @Override
    public void run() {
        readyThreadCounter.countDown();
        try {
            callingThreadBlocker.await();
            int passedDistance = 0;
            while (passedDistance < RACE_DISTANCE) {
                passedDistance += ThreadLocalRandom.current()
                        .nextInt(MIN_DISTANCE_PER_SNATCH, MAX_DISTANCE_PER_SNATCH + 1);
                long refreshTime = ThreadLocalRandom.current()
                        .nextInt(MIN_REFRESH_TIME, MAX_REFRESH_TIME + 1);
                try {
                    Thread.sleep(refreshTime);
                } catch (InterruptedException e) {
                    System.err.println("Thread " + Thread.currentThread().getName() + " caught interrupted exception");
                }
            }
            resultList.add(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            completedThreadCounter.countDown();
        }
    }

}
