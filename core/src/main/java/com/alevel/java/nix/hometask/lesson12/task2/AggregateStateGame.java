package com.alevel.java.nix.hometask.lesson12.task2;

import java.util.Scanner;

public class AggregateStateGame {

    public static void printMenu() {
        System.out.println("Choose substance to heat:");
        System.out.println("1 - Water");
        System.out.println("2 - Oxygen");
        System.out.println("3 - Iron");
        System.out.println("0 - Exit");
    }

    public static void cls() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

    public static void start() {
        Scanner scan = new Scanner(System.in);
        String str = "";
        int choice = 0;
        double temperature = 0;
        Substance water = new Water();
        Substance oxygen = new Oxygen();
        Substance iron = new Iron();
        State currentState;

        while (!"0".equals(str)) {
            printMenu();
            str = scan.next();
            try {
                choice = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input");
            }

            switch (choice) {
                case 1: {
                    System.out.println("Enter the temperature to heat or to cool:");
                    temperature = scan.nextDouble();
                    currentState = water.heatUp(temperature);
                    System.out.println("The water is " + water.getTemperature() + "C and in " + currentState.toString().toLowerCase() + " state");
                    break;
                }
                case 2: {
                    System.out.println("Enter the temperature to heat or to cool:");
                    temperature = scan.nextDouble();
                    currentState = oxygen.heatUp(temperature);
                    System.out.println("The oxygen is " + oxygen.getTemperature() + "C and in " + currentState.toString().toLowerCase() + " state");
                    break;
                }
                case 3: {
                    System.out.println("Enter the temperature to heat or to cool:");
                    temperature = scan.nextDouble();
                    currentState = iron.heatUp(temperature);
                    System.out.println("The iron is " + iron.getTemperature() + "C and in " + currentState.toString().toLowerCase() + " state");
                    break;
                }
                case 0:{
                    System.out.println("Exiting...");
                    break;
                }
                default: {
                    System.out.println("Choose one of the variants above.");
                }
            }


        }

    }

    public static void main(String[] args) {
        start();
    }
}
