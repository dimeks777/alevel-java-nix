package com.alevel.java.nix.reflection.csvmapper;

public class Person {

    @CSVColumn("name")
    private String name;

    @CSVColumn("age")
    private int age;

    @CSVColumn("gender")
    private String gender;

    @CSVColumn("occupation")
    private String occupation;

    @CSVColumn("salary")
    private double salary;

    @CSVColumn("isMarried")
    private boolean isMarried;

    public Person(String name, int age, String gender, String occupation, double salary, boolean isMarried) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.occupation = occupation;
        this.salary = salary;
        this.isMarried = isMarried;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }

    @Override
    public String toString() {
        return "Name:" + this.name + "\n"
                + "Age: " + this.age + "\n"
                + "Gender: " + this.gender + "\n"
                + "Occupation: " + this.occupation + "\n"
                + "Salary: " + this.salary + "\n"
                + "Is married: " + (this.isMarried ? "Yes" : "No") + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person that = (Person) o;
        return this.name.equals(that.name) && this.age == that.age
                && this.gender.equals(that.gender) && this.occupation.equals(that.occupation)
                && this.salary == that.salary && this.isMarried == that.isMarried;
    }
}
