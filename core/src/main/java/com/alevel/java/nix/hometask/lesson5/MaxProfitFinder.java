package com.alevel.java.nix.hometask.lesson5;


public class MaxProfitFinder {
    private int buyDay;
    private int sellDay;
    private double profit;

    MaxProfitFinder() {

    }

    MaxProfitFinder(int buyDay, int sellDay, double profit) {
        this.buyDay = buyDay;
        this.sellDay = sellDay;
        this.profit = profit;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getSellDay() {
        return sellDay;
    }

    public void setSellDay(int sellDay) {
        this.sellDay = sellDay;
    }

    public int getBuyDay() {
        return buyDay;
    }

    public void setBuyDay(int buyDay) {
        this.buyDay = buyDay;
    }

    public static MaxProfitFinder getMaxProfit(double[] stocksArr) {
        if (stocksArr != null) {
            double buyPrice = stocksArr[0];
            double maxProfit = stocksArr[1] - stocksArr[0];
            double currentPrice;
            double currentProfit;
            int buyIndex = 0;
            int sellIndex = 0;
            for (int i = 1, length = stocksArr.length; i < length; i++) {
                currentPrice = stocksArr[i];
                currentProfit = currentPrice - buyPrice;
                maxProfit = Math.max(maxProfit, currentProfit);
                if (maxProfit == currentProfit) sellIndex = i;
                buyPrice = Math.min(buyPrice, currentPrice);
                if (buyPrice == currentPrice) buyIndex = i;

            }
            maxProfit = maxProfit * 100;
            maxProfit = Math.round(maxProfit);
            maxProfit /= 100;

            return new MaxProfitFinder(buyIndex, sellIndex, maxProfit);
        } else {
            return null;
        }
    }

}