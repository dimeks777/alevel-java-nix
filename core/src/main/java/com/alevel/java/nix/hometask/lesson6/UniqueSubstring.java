package com.alevel.java.nix.hometask.lesson6;

public class UniqueSubstring {
    public static int getLongestUniqueSubstringLength(String str) {
        if (str == null || str.length() == 0) return 0;
        StringBuilder substring = new StringBuilder();
        int maxLength = 0;
        for (int i = 0, length = str.length(); i < length; i++) {

            if (substring.toString().contains(str.substring(i, i + 1))) {
                if (substring.length() > maxLength) maxLength = substring.length();
                substring.delete(0, substring.length());
                substring.append(str.charAt(i));

            } else {
                substring.append(str.charAt(i));
            }

        }
        return maxLength;
    }
}
