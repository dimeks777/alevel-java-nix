package com.alevel.java.nix.hometask.lesson11;

public interface TicTacToeGameLoop {

    void playGame();
}
