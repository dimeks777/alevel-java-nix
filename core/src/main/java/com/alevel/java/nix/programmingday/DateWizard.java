package com.alevel.java.nix.programmingday;

import java.time.LocalDate;

interface DateWizard {
    LocalDate getDateOfYear(int year, int day);

}
