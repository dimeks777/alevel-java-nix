package com.alevel.java.nix.hometask.lesson14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Retry<T> implements Block<T> {

    private static final Logger logger = LoggerFactory.getLogger(Retry.class);
    private int basicDelay;
    private Block<T> block;
    private int countOfAttempts;

    public Retry(int basicDelay, Block<T> block, int countOfAttempts) {
        this.basicDelay = basicDelay;
        this.block = block;
        this.countOfAttempts = countOfAttempts;
    }


    @Override
    public T run() throws Exception {
        Exception err = null;
        T temp = null;
        for (int i = 0; i < this.countOfAttempts; i++) {
            try {
                temp = block.run();
                return temp;
            } catch (Exception e) {
                err = e;
            }
            try {
                Thread.sleep(basicDelay * (i + 1));
            } catch (InterruptedException e) {
                logger.warn("Backoff interrupted, proceeding to retry", e);
            }
        }
        if (err != null) throw err;
        return null;
    }

}
