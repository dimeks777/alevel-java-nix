package com.alevel.java.nix.hometask.lesson13;

@FunctionalInterface
public interface Block {
    void run() throws Exception;
}
