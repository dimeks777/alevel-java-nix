package com.alevel.java.nix.exceptions;

public class ProgressionConfigurationException extends Exception {

    public static final String stepException = "Step of arithmetic progression can`t be zero";
    public static final String indexException = "Index of arithmetic progression can`t be lower than 1";

    public ProgressionConfigurationException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
