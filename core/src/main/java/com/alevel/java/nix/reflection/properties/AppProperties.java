package com.alevel.java.nix.reflection.properties;

public class AppProperties {

    @PropertyKey("name")
    public String name;

    @PropertyKey("connection.timeout")
    public int connectionTimeout;

    @PropertyKey("connection.limit")
    public int maxConnections;


    @Override
    public String toString() {
        return "App:\n" + "Name: " + name + "\n"
                + "Connection timeout: " + connectionTimeout + "\n"
                + "Connection limit: " + maxConnections + "\n";

    }

}
