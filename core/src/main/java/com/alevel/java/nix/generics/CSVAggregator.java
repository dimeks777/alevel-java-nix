package com.alevel.java.nix.generics;

import java.util.StringJoiner;

public class CSVAggregator<T> implements Aggregator<String, T> {
    @Override
    public String aggregate(T[] items) {
        if (items != null) {
            if (items.length == 0) return null;
            if (items.length == 1) return items[0].toString();
            StringJoiner stringJoiner = new StringJoiner(",");
            for (T elem : items) {
                stringJoiner.add(elem.toString());
            }
            return stringJoiner.toString();
        }
        return null;
    }
}
