package com.alevel.java.nix.reflection.csvmapper;

import com.alevel.java.nix.iopractice.assignment2.CSVTable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;

public class CSVMapper {

    public static <T> List<T> parse(CSVTable table, Class<T> t) throws Exception {
        try {
            List<T> result = new ArrayList<>();
            Field[] declaredFields = t.getDeclaredFields();
            Map<String, Field> annotationToField = new LinkedHashMap<>();

            for (Field declaredField : declaredFields) {
                CSVColumn annotation = declaredField.getAnnotation(CSVColumn.class);
                if (annotation != null && declaredField.trySetAccessible()) {
                    annotationToField.put(annotation.value(), declaredField);
                }
            }

            Constructor<T> constructor = t.getConstructor();

            for (int i = 0, size = table.getValues().size() / table.getHeaders().size(); i < size; i++) {
                T instance = constructor.newInstance();
                for (Map.Entry<String, Field> entry : annotationToField.entrySet()) {
                    String columnName = entry.getKey();
                    Field field = entry.getValue();
                    String value = table.accessValue(i, columnName);
                    Class<?> fieldType = field.getType();

                    if (fieldType.equals(String.class)) {
                        field.set(instance, value);
                    } else if (fieldType == Integer.class || fieldType == int.class) {
                        field.setInt(instance, Integer.parseInt(value));
                    } else if (fieldType == Long.class || fieldType == long.class) {
                        field.setLong(instance, Long.parseLong(value));
                    } else if (fieldType == Double.class || fieldType == double.class) {
                        field.setDouble(instance, Double.parseDouble(value));
                    } else if (fieldType == Boolean.class || fieldType == boolean.class) {
                        field.setBoolean(instance, Boolean.parseBoolean(value));
                    }
                }
                result.add(instance);
            }
            return result;
        } catch (Exception ignored) {
            throw new RuntimeException();
        }

    }
}
