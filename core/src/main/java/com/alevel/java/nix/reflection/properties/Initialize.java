package com.alevel.java.nix.reflection.properties;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class Initialize {

    public void initValues(Object object, Class<?> c) throws Exception {
        if (object != null) {
            Properties properties = loadProperties("/app.properties");
            for (Field field : c.getDeclaredFields()) {
                if (field.isAnnotationPresent(PropertyKey.class)) {
                    PropertyKey propertyKey = field.getAnnotation(PropertyKey.class);
                    field.setAccessible(true);
                    if (field.getType() == int.class) {
                        field.set(object, Integer.parseInt(properties.getProperty(propertyKey.value())));
                    } else {
                        field.set(object, properties.getProperty(propertyKey.value()));
                    }

                }
            }
        }
    }


    private static Properties loadProperties(String configPath) {
        var props = new Properties();
        try (var input = App.class.getResourceAsStream(configPath)) {
            props.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }
}
