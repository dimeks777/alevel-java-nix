package com.alevel.java.nix.hometask.lesson11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TicTacToeGameLoop3x3 implements TicTacToeGameLoop {

    private final TicTacToe3x3 model;
    private static final Logger logger = LoggerFactory.getLogger("com.alevel.java.nix.hometask.lesson11.TicTacToeGameLoop3x3");

    public TicTacToeGameLoop3x3(TicTacToe3x3 model) {
        this.model = model;
    }

    public void playGame() {
        model.initGame();
        do {
            model.playerMove();
            model.checkGameStatus(model.getLastSetSymbolRow(), model.getLastSetSymbolCol());
            model.getIo().printGame();
            if (model.getCurrentGameStatus() == TicTacToe3x3.PLAYER1WON) {
                System.out.println("Player1(X) won!");
                logger.info("Player1 won");
            } else if (model.getCurrentGameStatus() == TicTacToe3x3.PLAYER2WON) {
                System.out.println("Player2(O) won!");
                logger.info("Player2 won");
            } else if (model.getCurrentGameStatus() == TicTacToe3x3.DRAW) {
                System.out.println("Draw!");
                logger.info("Draw");
            }

            model.setCurrentPlayer((model.getCurrentPlayer() == TicTacToe3x3.PLAYER1) ? TicTacToe3x3.PLAYER2 : TicTacToe3x3.PLAYER1);
        } while (model.getCurrentGameStatus() == TicTacToe3x3.INGAME);
    }


}
