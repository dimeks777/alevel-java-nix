package com.alevel.java.nix.hometask.lesson7;


import java.util.Arrays;

public class ReverseCharSequenceSimple implements CharSequence {

    private final char[] original;

    public ReverseCharSequenceSimple(char[] original) {
        this.original = original;
    }

    public int length() {
        return original.length;
    }

    public char charAt(int index) {
        return original[index];
    }

    public String toString() {
        return new String(original);
    }

    public CharSequence subSequence(int start, int end) {
        return Arrays.toString(original).subSequence(start, end);
    }

    public static ReverseCharSequenceSimple reverse(CharSequence original) {
        if (original == null) return null;
        char[] arr = original.toString().toCharArray();
        if (arr.length < 2) return new ReverseCharSequenceSimple(arr);
        int left = 0, right = arr.length - 1;
        while (left < right) {
            char tmp = arr[left];
            arr[left++] = arr[right];
            arr[right--] = tmp;
        }
        return new ReverseCharSequenceSimple(arr);
    }


}
