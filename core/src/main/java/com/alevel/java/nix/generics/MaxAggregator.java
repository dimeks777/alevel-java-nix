package com.alevel.java.nix.generics;

public class MaxAggregator<T extends Comparable<? super T>> implements Aggregator<T, T> {

    @Override
    public T aggregate(T[] items) {
        if (items != null) {
            if (items.length == 0) return null;
            if (items.length == 1) return items[0];
            T max = items[0];
            for (int i = 1; i < items.length; i++) {
                if (items[i].compareTo(max) > 0) {
                    max = items[i];
                }
            }
            return max;
        }
        return null;

    }

}
