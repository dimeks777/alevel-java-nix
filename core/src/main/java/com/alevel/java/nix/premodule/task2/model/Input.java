package com.alevel.java.nix.premodule.task2.model;

import com.alevel.java.nix.premodule.task2.HangmanRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class Input {

    private static final Logger logger = LoggerFactory.getLogger(Input.class);

    public static String chooseWord(String cp) {
        List<String> list = null;
        try {
            URL fileURL = HangmanRunner.class.getResource(cp);
            File f = new File(fileURL.getFile());
            list = Files.readAllLines(f.toPath());
            Random random = new Random();
            return list.get(random.nextInt(list.size()));
        } catch (Exception e) {
            logger.error("An error occurred while loading a game: {}", e.getMessage());
        }
        return null;
    }

}
