package com.alevel.java.nix.hometask.lesson4;


public class FizzBuzz {


    public String getStringRepresentationByDivisorLeft(int number) {
        int power = 0;
        int temp;
        StringBuilder stringBuilder = new StringBuilder();
        if (number != 0) {
            number = Math.abs(number);
            temp = number;
            while (temp > 0) {
                temp /= 10;
                power++;
            }
            while (number > 0) {
                temp = (int) (number / Math.pow(10, power - 1));
                checkIfDivisible(temp, stringBuilder);
                number %= Math.pow(10, power - 1);
                power--;

            }
            return stringBuilder.toString();
        } else {
            return "fizzbuzz\n";
        }

    }


    public String getStringRepresentationByDivisorRight(int number) {
        int temp;
        StringBuilder stringBuilder = new StringBuilder();
        if (number != 0) {
            number = Math.abs(number);
            while (number > 0) {
                temp = number % 10;
                number = number / 10;
                checkIfDivisible(temp, stringBuilder);
            }

            return stringBuilder.toString();


        } else {
            return "fizzbuzz\n";
        }
    }

    private void checkIfDivisible(int temp, StringBuilder stringBuilder) {
        boolean divisibleByTwo;
        boolean divisibleByThree;
        divisibleByTwo = temp % 2 == 0;
        divisibleByThree = temp % 3 == 0;
        if (divisibleByTwo && divisibleByThree) {
            stringBuilder.append("fizzbuzz\n");
        } else if (divisibleByTwo) {
            stringBuilder.append("fizz\n");
        } else if (divisibleByThree) {
            stringBuilder.append("buzz\n");
        } else {
            stringBuilder.append(Integer.toString(temp)).append("\n");

        }
    }

}
