package com.alevel.java.nix.connector;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConnectorTest {

    @Test
    void getConnection() {
        Connection connection = Connector.getConnection("/datasource.properties");
        assertNotNull(connection);
        try {
            connection.close();
        } catch (SQLException ignored) {

        }

    }
}