package com.alevel.java.nix.dijkstra;

import com.alevel.java.nix.connector.Connector;
import com.alevel.java.nix.tasks.task3.City;
import com.alevel.java.nix.tasks.task3.Road;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ShortestPathIO {
    private final String configPath;
    private static final Logger log = LoggerFactory.getLogger(ShortestPathIO.class);

    public ShortestPathIO(String configPath) {
        this.configPath = configPath;
    }

    public SimpleDijkstra read() {

        try (Connection root = Connector.getConnection(configPath)) {
            List<Map<Integer, Integer>> routes = new ArrayList<>();
            List<City> cities;
            int size;

            try (PreparedStatement problemsList = root.prepareStatement(
                    "SELECT from_city_id, to_city_id FROM problems")) {
                ResultSet problemList = problemsList.executeQuery();
                int i = 1;
                while (problemList.next()) {
                    int from = problemList.getInt(1);
                    int to = problemList.getInt(2);
                    log.info("Route to find {} - {}, problem {}", from, to, i++);
                    routes.add(Map.of(from - 1, to - 1));
                }
            }

            try (PreparedStatement cityList = root.prepareStatement(
                    "SELECT count(*) FROM city  as cnt")) {
                ResultSet city = cityList.executeQuery();
                city.next();
                size = city.getInt(1);
                cities = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    cities.add(new City(i, null));
                }

            }
            try (PreparedStatement roadList = root.prepareStatement(
                    "SELECT * FROM connection ORDER BY from_city_id")) {
                ResultSet connections = roadList.executeQuery();
                List<Road> roads = new ArrayList<>();
                int id = 0;

                while (connections.next()) {
                    int curId = connections.getInt(2) - 1;
                    if (curId != id) {
                        cities.get(id).setRoads(roads);
                        roads = new ArrayList<>();
                        id = curId;
                    }
                    roads.add(new Road(cities.get(connections.getInt(3) - 1), connections.getInt(1)));
                }
                cities.get(id).setRoads(roads);
            }

            try (PreparedStatement names = root.prepareStatement(
                    "SELECT name FROM city ORDER BY city_id")) {
                ResultSet n = names.executeQuery();
                int i = 0;
                while (n.next()) {
                    cities.get(i++).setName(n.getString(1));
                }
            }
            return new SimpleDijkstra(cities, routes);

        } catch (SQLException e) {
            log.error("SQL error occurred: {}", e.getErrorCode());
        }
        log.error("Unable to connect to database pathfinder");

        return null;
    }

    public void writeToDB(Collection<Map<Integer, Integer>> collection) {

        try (Connection root = Connector.getConnection(configPath)) {
            for (Map<Integer, Integer> map : collection) {
                for (var entry : map.entrySet()) {
                    int temp;
                    if ((temp = entry.getValue()) != 0) {
                        try (PreparedStatement insertValue = root.prepareStatement(
                                "INSERT IGNORE INTO found_routes (problem_id, min_cost) VALUES (?, ?);",
                                PreparedStatement.RETURN_GENERATED_KEYS
                        )) {
                            int key = entry.getKey();
                            insertValue.setInt(1, key);
                            insertValue.setInt(2, temp);
                            insertValue.addBatch();
                            insertValue.executeBatch();
                            log.info("Inserting solution for problem {} : {}", key, temp);
                        }
                    } else {
                        try (PreparedStatement insertValue = root.prepareStatement(
                                "INSERT IGNORE INTO impossible_routes (problem_id) VALUES (?);",
                                PreparedStatement.RETURN_GENERATED_KEYS
                        )) {
                            insertValue.setInt(1, entry.getKey());
                            insertValue.addBatch();
                            insertValue.executeBatch();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            log.error("SQL error occurred: {}", e.getErrorCode());
        }
    }

}
