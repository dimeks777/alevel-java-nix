package com.alevel.java.nix.connector;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {

    public static Connection getConnection(String configPath) {
        var props = loadProperties(configPath);
        String url = props.getProperty("mysql.database.url");
        String user = props.getProperty("mysql.user");
        String password = props.getProperty("mysql.password");
        Connection root = null;
        try {
            root = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return root;
    }

    private static Properties loadProperties(String configPath) {
        var props = new Properties();
        try (var input = Connector.class.getResourceAsStream(configPath)) {
            props.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

}
