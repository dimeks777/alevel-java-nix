package com.alevel.java.nix.dijkstra;

import com.alevel.java.nix.tasks.task3.City;
import com.alevel.java.nix.tasks.task3.Road;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class SimpleDijkstra {

    private final List<City> cities;
    private final List<Map<Integer, Integer>> neededPaths;
    private List<Map<Integer, Integer>> result = new ArrayList<>();

    public SimpleDijkstra(List<City> cities, List<Map<Integer, Integer>> neededPaths) {
        this.cities = cities;
        this.neededPaths = neededPaths;
    }

    public List<City> getCities() {
        return cities;
    }

    public List<Map<Integer, Integer>> getNeededPaths() {
        return neededPaths;
    }

    public List<Map<Integer, Integer>> getResult() {
        return result;
    }

    public static void getAllPaths(City source) {

        source.setMinCost(0);
        PriorityQueue<City> cityPriorityQueue = new PriorityQueue<>();
        cityPriorityQueue.add(source);
        while (!cityPriorityQueue.isEmpty()) {
            City src = cityPriorityQueue.poll();

            for (Road road : src.getRoads()) {
                City dest = road.getDest();
                int cost = road.getCost();
                int currentCost = src.getMinCost() + cost;
                if (currentCost < dest.getMinCost()) {
                    cityPriorityQueue.remove(dest);
                    dest.setMinCost(currentCost);
                    cityPriorityQueue.add(dest);
                }
            }
        }
    }

    private int findShortestPath(City src, City dest) {
        getAllPaths(src);
        return dest.getMinCost();
    }

    public List<Map<Integer, Integer>> result() {

        if (neededPaths != null && cities != null) {
            int i = 1;
            for (Map<Integer, Integer> route : neededPaths) {
                for (var entry : route.entrySet()) {
                    int path = findShortestPath(cities.get(entry.getKey()), cities.get(entry.getValue()));
                    if (path != Integer.MAX_VALUE) {
                        result.add(Map.of(i++, path));
                    } else {
                        result.add(Map.of(i++, 0));
                    }
                }
            }
            return result;
        }
        return null;
    }

}

