package com.alevel.java.nix.dijkstra;

import java.util.List;
import java.util.Map;

public class ShortestPathRunner {
    public static void main(String[] args) {
        ShortestPathIO io = new ShortestPathIO("/datasource.properties");
        SimpleDijkstra simpleDijkstra = io.read();
        if (simpleDijkstra != null) {
            List<Map<Integer, Integer>> list = simpleDijkstra.result();
            io.writeToDB(list);
        }
    }
}
