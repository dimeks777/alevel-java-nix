package com.alevel.java.nix.moneymanager.controller;

import com.alevel.java.nix.moneymanager.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public class HibernateTransactionHandler {
    private static final Logger logger = LoggerFactory.getLogger(HibernateTransactionHandler.class);
    private final Session session;
    private final User user;

    public HibernateTransactionHandler(SessionFactory sessionFactory, Long userID) {
        this.session = sessionFactory.openSession();
        this.user = getUser(userID);
    }

    public void addOperation() {
        try {
            Operation newOperation = Input.createOperation(this);
            if (newOperation == null) {
                logger.error("Operation add failed or user stopped it");
                return;
            }
            session.beginTransaction();
            session.save(newOperation);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction", e);
            session.getTransaction().rollback();
        }
    }

    public void close() {
        session.close();
    }

    public Account getAccount(Long id) {
        Account account = null;
        try {
            session.beginTransaction();
            account = session.find(Account.class, id);
            String query = String.format("from Account WHERE user_id = %d AND id = %d ", user.getId(), id);
            Query<Account> accountQuery = session.createQuery(query, Account.class);
            try {
                account = accountQuery.getSingleResult();
            } catch (NoResultException e) {
                logger.error("No account found with id {}, or this is not your's", id);
                return null;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction: {}", e.getMessage());
            session.getTransaction().rollback();
        }
        return account;
    }

    public User getUser(Long id) {
        User user = null;
        try {
            session.beginTransaction();
            user = session.find(User.class, id);
            if (user == null) {
                logger.warn("No user found with id {}", id);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction", e);
            session.getTransaction().rollback();
        }
        return user;
    }

    public boolean checkCategories(List<String> categories, Class<? extends Category> categoryType) {
        try {
            session.beginTransaction();
            for (String category : categories) {
                String query = String.format("from Category WHERE name = '%s' ", category.toLowerCase());
                if (session.createQuery(query, Category.class) == null) return false;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction", e);
            session.getTransaction().rollback();
        }
        return true;
    }

    public List<String> getCategories() {
        List<String> categories = new ArrayList<>();
        try {
            session.beginTransaction();
            Query<ConsumptionCategory> consumptionCategories =
                    session.createQuery("from ConsumptionCategory ", ConsumptionCategory.class);
            List<ConsumptionCategory> listConsumptionCategories = consumptionCategories.list();

            Query<ProfitCategory> profitCategories =
                    session.createQuery("from ProfitCategory ", ProfitCategory.class);
            List<ProfitCategory> listProfitCategories = profitCategories.list();

            for (ProfitCategory profitCategory : listProfitCategories) {
                categories.add(profitCategory.getCategoryID().getName());
            }

            for (ConsumptionCategory consumptionCategory : listConsumptionCategories) {
                categories.add(consumptionCategory.getCategoryID().getName());
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction", e);
            session.getTransaction().rollback();
        }
        return categories;
    }

    public Category getCategory(String name, Class<? extends Category> c) {
        Category category = null;
        try {
            String query = "";
            session.beginTransaction();
            Query<Category> categoryQuery;
            if (c == ProfitCategory.class) {
                query = String.format("from Category WHERE name = '%s' AND DTYPE = 'ProfitCategory'", name);
            } else if (c == ConsumptionCategory.class) {
                query = String.format("from Category WHERE name = '%s' AND DTYPE = 'ConsumptionCategory'", name);
            }
            categoryQuery = session.createQuery(query, Category.class);
            try {
                category = categoryQuery.getSingleResult();
            } catch (NoResultException e) {
                logger.error("No category matches for given type");
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction", e);
            session.getTransaction().rollback();
        }
        return category;
    }


}
