package com.alevel.java.nix.moneymanager.model;

import javax.persistence.*;

@Entity
public abstract class Category {

    @EmbeddedId
    private CategoryID categoryID;

    public CategoryID getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(CategoryID categoryID) {
        this.categoryID = categoryID;
    }
}
