package com.alevel.java.nix.moneymanager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class LoadDefaultData {
    private static final Logger logger = LoggerFactory.getLogger(LoadDefaultData.class);

    public static void load(SessionFactory sessionFactory) throws IOException {
        try (Session session = sessionFactory.openSession()) {
            List<String> list = readSQLScript(LoadDefaultData.class.getResource("/initialize.sql").getPath());
            if (list != null) {
                try {
                    session.beginTransaction();
                    for (String s : list) {
                        session.createNativeQuery(s).executeUpdate();
                    }
                    session.getTransaction().commit();
                } catch (Exception e) {
                    logger.error("Error while executing initial script");
                    session.getTransaction().rollback();
                }
            }
        }
    }

    private static List<String> readSQLScript(final String pathToFile) throws IOException {
        List<String> statements = new ArrayList<>();
        FileInputStream fileReader = new FileInputStream(pathToFile);
        byte[] buffer = new byte[fileReader.available()];
        if (fileReader.read(buffer, 0, fileReader.available()) == -1) {
            logger.error("Failed to read file {}!", pathToFile);
            return null;
        }
        StringBuilder query = new StringBuilder();
        for (byte b : buffer) {
            query.append((char) b);
        }
        String filteredQuery = query.toString();
        filteredQuery = filteredQuery.replace("\n", " ").replaceAll("\\s{2,}", "");
        StringTokenizer stringTokenizer = new StringTokenizer(filteredQuery, ";");
        while (stringTokenizer.hasMoreTokens()) {
            statements.add(stringTokenizer.nextToken() + ";");
        }
        return statements;
    }
}
