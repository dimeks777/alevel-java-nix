package com.alevel.java.nix.moneymanager.controller;

import com.alevel.java.nix.moneymanager.DataValidator;
import com.alevel.java.nix.moneymanager.model.*;
import com.alevel.java.nix.moneymanager.view.MenuView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Input {
    private static final Logger logger = LoggerFactory.getLogger(Input.class);
    private static final Scanner scanner = new Scanner(System.in);
    private static final DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static Operation createOperation(HibernateTransactionHandler hth) {
        Operation operation = new Operation();
        operation.setInstant(Instant.now().truncatedTo(ChronoUnit.SECONDS).plusSeconds(10800));
        List<String> inputCategories = new ArrayList<>();
        int choice;
        while (true) {
            MenuView.printAddOperation();
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    Account account = hth.getAccount(inID(scanner));
                    operation.setAccount(account);
                    break;
                case 2:
                    MenuView.printCategories(hth.getCategories());
                    System.out.println("Enter payment appointment:");
                    inputCategories.add(new Scanner(System.in).nextLine());
                    break;
                case 3:
                    operation.setTransactionValue(inTransactionValue(scanner));
                    break;
                case 4:
                    if (DataValidator.checkOperationNotNull(operation) && inputCategories.size() != 0) {
                        if (operation.getTransactionValue().compareTo(BigDecimal.ZERO) > 0) {
                            if (hth.checkCategories(inputCategories, ProfitCategory.class)) {
                                for (String category : inputCategories) {
                                    Category newCategory = hth.getCategory(category, ProfitCategory.class);
                                    if (newCategory == null) {
                                        logger.error("Selected different category types");
                                        return null;
                                    }
                                    operation.addCategory(newCategory);
                                }
                                BigDecimal previous = operation.getAccount().getBalance();
                                operation.getAccount().setBalance(previous.add(operation.getTransactionValue()));
                            }
                        } else if (operation.getTransactionValue().compareTo(BigDecimal.ZERO) < 0) {
                            if (hth.checkCategories(inputCategories, ConsumptionCategory.class)) {
                                BigDecimal previous = BigDecimal.ZERO;
                                previous = previous.add(operation.getAccount().getBalance());
                                BigDecimal current = previous.add(operation.getTransactionValue());
                                if (current.compareTo(BigDecimal.ZERO) >= 0) {
                                    operation.getAccount().setBalance(current);
                                    for (String category : inputCategories) {
                                        operation.addCategory(hth.getCategory(category, ConsumptionCategory.class));
                                    }
                                } else {
                                    logger.error("Not enough money on balance: \n Current: {} need: {} ",
                                            previous, operation.getTransactionValue().abs());
                                    return null;
                                }
                            }
                        } else {
                            logger.error("Invalid operation");
                            return null;
                        }
                        return operation;
                    } else {
                        logger.warn("Not all fields are entered");
                    }
                    break;
                case 5:
                    return null;
            }
        }
    }

    public static Long inID(Scanner scanner) {
        System.out.println("Enter account ID:");
        boolean acceptable = false;
        long value = 0L;
        while (!acceptable) {
            try {
                value = scanner.nextLong();
                acceptable = true;
            } catch (InputMismatchException e) {
                logger.warn("Incorrect input");
            }
        }
        return value;
    }

    public static BigDecimal inTransactionValue(Scanner scanner) {
        System.out.println("Enter transaction value:");
        boolean acceptable = false;
        BigDecimal value = null;
        while (!acceptable) {
            try {
                value = new BigDecimal(scanner.next());
                acceptable = true;
            } catch (InputMismatchException e) {
                logger.warn("Incorrect input");
            }
        }
        return value;
    }

    public static LocalDate inDate(Scanner scanner) {
        boolean acceptable = false;
        LocalDate localDate = null;
        while (!acceptable) {
            try {
                System.out.println("Enter date in format dd-MM-yyyy");
                String date = scanner.next();
                localDate = LocalDate.parse(date, pattern);
                acceptable = true;
            } catch (DateTimeParseException ignored) {

            }
        }
        return localDate;
    }
}
