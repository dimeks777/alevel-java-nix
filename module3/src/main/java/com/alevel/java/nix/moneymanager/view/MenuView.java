package com.alevel.java.nix.moneymanager.view;

import java.util.List;
import java.util.Scanner;

public class MenuView {

    public static void printMainMenu() {
        System.out.println("Select item(1-3):\n"
                + "1 - Add new operation\n"
                + "2 - Export orderings\n"
                + "3 - Exit");
    }

    public static void cls() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

    public static void waitForEnter() {
        System.out.println("Enter any key to continue...");
        new Scanner(System.in).nextLine();
    }

    public static void printAddOperation() {
        System.out.println("Input menu:\n" +
                "1 - Account ID\n" +
                "2 - Operation category\n" +
                "3 - Transaction value\n" +
                "4 - Confirm and add operation\n" +
                "5 - Exit");
    }

    public static void printAddCategory() {
        System.out.println("Category type:\n" +
                "1 - Profit\n" +
                "2 - Consumption");
    }

    public static void printCategories(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s).append("\n");
        }
        System.out.println(sb);
    }

}
