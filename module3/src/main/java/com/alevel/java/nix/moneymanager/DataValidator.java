package com.alevel.java.nix.moneymanager;

import com.alevel.java.nix.moneymanager.model.Operation;


public class DataValidator {

    private static final String EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")" +
            "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
            "|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}" +
            "(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private static final String ID = "\\d{10}";
    private static final String TEL_NUMBER = "^\\+?3?8?(0\\d{9})$";

    public static boolean checkEmail(String input) {
        return input.matches(EMAIL);
    }

    public static boolean checkID(String input) {
        return input.matches(ID);
    }

    public static boolean checkTelephoneNumber(String input) {
        return input.matches(TEL_NUMBER);
    }

    public static boolean checkOperationNotNull(Operation operation) {
        return operation.getAccount() != null
                && operation.getTransactionValue() != null
                && operation.getInstant() != null;
    }

}
