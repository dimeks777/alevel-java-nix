package com.alevel.java.nix.moneymanager.controller;


import com.alevel.java.nix.moneymanager.model.CSVFileLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JDBCTransactionHandler {

    private static final Logger logger = LoggerFactory.getLogger(JDBCTransactionHandler.class);
    private static final String pathToConfig = "/datasource.properties";


    private List<CSVFileLine> getOperationList(LocalDate start, LocalDate end, Long accountID) {
        try (Connection root = Connector.getConnection(pathToConfig)) {
            List<CSVFileLine> operations = new ArrayList<>();
            String query = String.format("  SELECT operation.id,transactionValue, GROUP_CONCAT(oc.categories_name\n" +
                    "                                        ORDER BY oc.categories_name ASC\n" +
                    "                                         SEPARATOR ', ') AS categories,instant \n" +
                    " FROM operation LEFT JOIN operation_category oc on operation.id = oc.Operation_id\n" +
                    "    LEFT JOIN account on operation.account_id = account.id WHERE account_id = %d AND instant > '%s' \n" +
                    "                                                             AND instant < '%s' GROUP BY operation.id", accountID, start, end);
            try (PreparedStatement operationStatement = root.prepareStatement(query)) {
                ResultSet operationList = operationStatement.executeQuery(query);
                BigDecimal profits = BigDecimal.ZERO;
                BigDecimal consumptions = BigDecimal.ZERO;
                while (operationList.next()) {
                    Long operationId = operationList.getLong(1);
                    BigDecimal transactionValue = operationList.getBigDecimal(2);
                    String categoryName = operationList.getObject(3, String.class);
                    String instant = operationList.getObject(4, String.class);
                    operations.add(new CSVFileLine(operationId, transactionValue, categoryName, instant));
                    if (transactionValue.compareTo(BigDecimal.ZERO) > 0) profits = profits.add(transactionValue);
                    else consumptions = consumptions.add(transactionValue);
                }
                CSVFileLine lastLine = new CSVFileLine();
                lastLine.setProfit(profits);
                lastLine.setBalance(profits.add(consumptions));
                operations.add(lastLine);
                return operations;
            }
        } catch (SQLException e) {
            logger.error("An error occurred", e);
        }
        return null;
    }


    public void CSVMenu(Long userID) {
        try (Connection root = Connector.getConnection(pathToConfig)) {

            String query = String.format("SELECT id FROM account WHERE user_id = %d", userID);
            try (PreparedStatement accountQuery = root.prepareStatement(query)) {
                ResultSet accountList = accountQuery.executeQuery(query);
                while (accountList.next()) {
                    Long accountId = accountList.getLong(1);
                    System.out.println(accountId);
                }
                System.out.println("Enter account to get CSV");
                Long id = new Scanner(System.in).nextLong();
                new CSVWriter().write(getDatesAndGetList(id));
            }
        } catch (SQLException e) {
            logger.error("An error occurred", e);
        }
    }

    private List<CSVFileLine> getDatesAndGetList(Long id) {
        System.out.println("Enter start date:");
        Scanner scanner = new Scanner(System.in);
        LocalDate start = Input.inDate(scanner);
        System.out.println("Enter end date:");
        LocalDate end = Input.inDate(scanner);
        return getOperationList(start, end, id);
    }
}
