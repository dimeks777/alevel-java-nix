package com.alevel.java.nix.moneymanager.model;

import java.math.BigDecimal;
import java.util.Objects;

public class CSVFileLine {
    private Long operationId;
    private BigDecimal transactionValue;
    private String categoryName;
    private String instant;
    private BigDecimal profit;
    private BigDecimal balance;

    public CSVFileLine() {
    }

    public CSVFileLine(Long operationId, BigDecimal transactionValue, String categoryName, String instant) {
        this.operationId = operationId;
        this.transactionValue = transactionValue;
        this.categoryName = categoryName;
        this.instant = instant;
    }

    public CSVFileLine(Long operationId, BigDecimal transactionValue, String categoryName, String instant,
                       BigDecimal profit, BigDecimal balance) {
        this.operationId = operationId;
        this.transactionValue = transactionValue;
        this.categoryName = categoryName;
        this.instant = instant;
        this.profit = profit;
        this.balance = balance;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }


    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public BigDecimal getTransactionValue() {
        return transactionValue;
    }

    public void setTransactionValue(BigDecimal transactionValue) {
        this.transactionValue = transactionValue;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getInstant() {
        return instant;
    }

    public void setInstant(String instant) {
        this.instant = instant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CSVFileLine)) return false;
        CSVFileLine that = (CSVFileLine) o;
        return getOperationId().equals(that.getOperationId()) &&
                getTransactionValue().equals(that.getTransactionValue()) &&
                getCategoryName().equals(that.getCategoryName()) &&
                getInstant().equals(that.getInstant());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOperationId(), getTransactionValue(), getCategoryName(), getInstant());
    }

    public String toString() {
        return (operationId == null ? " " : operationId) + "," + (transactionValue == null ? " " : transactionValue) + ","
                + (categoryName == null ? " " : categoryName) + "," + (instant == null ? " " : instant) + ","
                + (profit == null ? " " : profit) + "," + (balance == null ? " " : balance) + "," + "\n";
    }
}
