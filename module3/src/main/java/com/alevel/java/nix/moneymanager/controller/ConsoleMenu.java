package com.alevel.java.nix.moneymanager.controller;

import com.alevel.java.nix.moneymanager.view.MenuView;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class ConsoleMenu {
    private static final Logger logger = LoggerFactory.getLogger(ConsoleMenu.class);
    private final Scanner scanner = new Scanner(System.in);

    public void start(SessionFactory sessionFactory, Long userID) {
        int input;
        HibernateTransactionHandler hth = new HibernateTransactionHandler(sessionFactory, userID);
        JDBCTransactionHandler jth = new JDBCTransactionHandler();
        while (true) {
            MenuView.printMainMenu();
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    hth.addOperation();
                    break;
                case 2:
                    jth.CSVMenu(userID);
                    break;
                case 3:
                    hth.close();
                    return;
                default:
                    logger.warn("Invalid input");
            }
            MenuView.waitForEnter();
            MenuView.cls();
        }
    }
}
