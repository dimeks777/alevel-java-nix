package com.alevel.java.nix.moneymanager.controller;

import com.alevel.java.nix.moneymanager.model.CSVFileLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class CSVWriter {
    private static final Logger logger = LoggerFactory.getLogger(CSVWriter.class);
    private static final String defaultPath = "/output.csv";
    private static final String headers = "id,transactionValue,categoryName,instant,profit,balance,\n";

    public void write(List<CSVFileLine> lines) {
        File file = new File(JDBCTransactionHandler.class.getResource(defaultPath).getPath());
        try {
            Writer writer = new FileWriter(file);
            writer.write(headers);
            for (CSVFileLine csvFileLine : lines) {
                String s = csvFileLine.toString();
                writer.write(s);
            }
            writer.flush();
        } catch (IOException e) {
            logger.error("Error occurred while writing CSV", e);
        }

    }
}
