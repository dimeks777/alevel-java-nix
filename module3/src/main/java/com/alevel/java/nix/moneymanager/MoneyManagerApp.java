package com.alevel.java.nix.moneymanager;

import com.alevel.java.nix.moneymanager.controller.ConsoleMenu;
import com.alevel.java.nix.moneymanager.controller.HibernateTransactionHandler;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MoneyManagerApp {
    private static final Logger logger = LoggerFactory.getLogger(MoneyManagerApp.class);

    public static void main(String[] args) throws IOException {

        if (args.length == 3) {

            Long userID = Long.valueOf(args[0]);
            Configuration cfg = new Configuration().configure();
            cfg.getProperties().setProperty("hibernate.connection.username", args[1]);
            cfg.getProperties().setProperty("hibernate.connection.password", args[2]);
            try {
                SessionFactory sessionFactory = cfg.buildSessionFactory();
                LoadDefaultData.load(sessionFactory);
                HibernateTransactionHandler hth = new HibernateTransactionHandler(sessionFactory, userID);
                if (hth.getUser(Long.valueOf(args[0])) != null) {
                    new ConsoleMenu().start(sessionFactory, userID);
                } else {
                    logger.error("Invalid id");
                }
                hth.close();
                sessionFactory.close();
            } catch (Exception e) {
                logger.error("Invalid username or password");
            }

        } else {
            logger.error("Incorrect count of args: need 3, actual {}", args.length);
        }

    }
}
