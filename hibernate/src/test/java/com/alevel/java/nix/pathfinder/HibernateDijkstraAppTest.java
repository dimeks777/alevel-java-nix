package com.alevel.java.nix.pathfinder;

import com.alevel.java.nix.dijkstra.SimpleDijkstra;
import com.alevel.java.nix.pathfinder.entity.*;
import com.alevel.java.nix.pathfinder.io.DijkstraFromFile;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HibernateDijkstraAppTest {

    @Test
    public void run() {
        Cities gdansk = new Cities(1L, "gdansk");
        Cities bydgoszcz = new Cities(2L, "bydgoszcz");
        Cities torun = new Cities(3L, "torun");
        Cities warszawa = new Cities(4L, "warszawa");
        Cities kharkiv = new Cities(5L, "kharkiv");
        List<Cities> cities = List.of(gdansk, bydgoszcz, torun, warszawa, kharkiv);
        List<Connection> connections = List.of(new Connection(new ConnectionId(gdansk, bydgoszcz, 1))
                , new Connection(new ConnectionId(gdansk, torun, 3)), new Connection(new ConnectionId(bydgoszcz, gdansk, 1))
                , new Connection(new ConnectionId(bydgoszcz, torun, 1)), new Connection(new ConnectionId(bydgoszcz, warszawa, 4))
                , new Connection(new ConnectionId(torun, gdansk, 3)), new Connection(new ConnectionId(torun, bydgoszcz, 1))
                , new Connection(new ConnectionId(torun, warszawa, 1)), new Connection(new ConnectionId(warszawa, bydgoszcz, 4))
                , new Connection(new ConnectionId(warszawa, torun, 1)));
        Problems problem1 = new Problems(1L, gdansk, warszawa);
        Problems problem2 = new Problems(2L, bydgoszcz, warszawa);
        Problems problem3 = new Problems(3L, torun, kharkiv);
        List<Problems> problems = List.of(problem1, problem2, problem3);
        List<FoundRoutes> foundRoutes = List.of(new FoundRoutes(1L, problem1, 3),
                new FoundRoutes(2L, problem2, 2));
        List<ImpossibleRoutes> impossibleRoutes = List.of(new ImpossibleRoutes(3L, problem3));

        Configuration cfg = new Configuration().configure();
        try (SessionFactory sessionFactory = cfg.buildSessionFactory();
             Session session = sessionFactory.openSession()
        ) {
            SimpleDijkstra simpleDijkstra = DijkstraFromFile.fileParser("/input.txt");
            if (simpleDijkstra != null) {
                List<Map<Integer, Integer>> list = simpleDijkstra.result();
                HibernateDijkstraApp.writeEntities(session, simpleDijkstra, list);
            }

            List<Cities> actualCities = session
                    .createQuery("from Cities ", Cities.class)
                    .list();
            List<Connection> actualConnections = session
                    .createQuery("from Connection", Connection.class)
                    .list();
            List<Problems> actualProblems = session
                    .createQuery("from Problems", Problems.class)
                    .list();
            List<FoundRoutes> actualFoundRouts = session
                    .createQuery("from FoundRoutes", FoundRoutes.class)
                    .list();
            List<ImpossibleRoutes> actualImpossibleRouts = session
                    .createQuery("from ImpossibleRoutes", ImpossibleRoutes.class)
                    .list();

            for (int i = 0, length = cities.size(); i < length; i++) {
                assertEquals(cities.get(i).getId(), actualCities.get(i).getId());
                assertEquals(cities.get(i).getName(), actualCities.get(i).getName());
            }


            actualConnections.sort(Comparator.comparingLong(p -> p.getId().getFromCity().getId()));

            for (int i = 0, length = connections.size(); i < length; i++) {
                assertEquals(connections.get(i).getId().getFromCity().getName(), actualConnections.get(i).getId().getFromCity().getName());
                assertEquals(connections.get(i).getId().getToCity().getName(), actualConnections.get(i).getId().getToCity().getName());
                assertEquals(connections.get(i).getId().getCost(), actualConnections.get(i).getId().getCost());
            }

            for (int i = 0, length = problems.size(); i < length; i++) {
                assertEquals(problems.get(i).getFromCity().getName(), actualProblems.get(i).getFromCity().getName());
                assertEquals(problems.get(i).getToCity().getName(), actualProblems.get(i).getToCity().getName());
            }

            for (int i = 0, length = foundRoutes.size(); i < length; i++) {
                assertEquals(foundRoutes.get(i).getMinCost(), actualFoundRouts.get(i).getMinCost());
                assertEquals(foundRoutes.get(i).getProblem().getFromCity().getName(), actualFoundRouts.get(i).getProblem().getFromCity().getName());
                assertEquals(foundRoutes.get(i).getProblem().getToCity().getName(), actualFoundRouts.get(i).getProblem().getToCity().getName());
            }

            for (int i = 0, length = impossibleRoutes.size(); i < length; i++) {
                assertEquals(impossibleRoutes.get(i).getProblem().getFromCity().getName(), actualImpossibleRouts.get(i).getProblem().getFromCity().getName());
                assertEquals(impossibleRoutes.get(i).getProblem().getToCity().getName(), actualImpossibleRouts.get(i).getProblem().getToCity().getName());
            }
        }
    }

}