package com.alevel.java.nix.etlapp.input;

import com.alevel.java.nix.etlapp.TestCompare;
import com.alevel.java.nix.etlapp.model.Database;
import org.junit.jupiter.api.Test;

class FileParserTest {

    private final TestCompare testCompare = new TestCompare();

    @Test
    void getDatabaseFromResourceFile() {
        Database expected = testCompare.initialize();
        Database actual = FileParser.getDatabaseFromResourceFile("/input.txt");
        testCompare.assertDatabases(expected, actual);
    }
}