package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.dijkstra.SimpleDijkstra;
import com.alevel.java.nix.tasks.task3.City;
import com.alevel.java.nix.tasks.task3.Road;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class DijkstraFromFileTest {

    @Test
    void fileParser() {
        City gdansk = new City(1, "gdansk");
        City bydgoszcz = new City(2, "bydgoszcz");
        City torun = new City(3, "torun");
        City warszawa = new City(4, "warszawa");
        City kharkiv = new City(5, "kharkiv");

        gdansk.setRoads(List.of(
                new Road(bydgoszcz, 1),
                new Road(torun, 3)));
        bydgoszcz.setRoads(List.of(new Road(gdansk, 1),
                new Road(torun, 1),
                new Road(warszawa, 4)
        ));
        torun.setRoads(List.of(new Road(gdansk, 3),
                new Road(bydgoszcz, 1),
                new Road(warszawa, 1)
        ));
        warszawa.setRoads(List.of(new Road(gdansk, 4),
                new Road(torun, 1)
        ));
        kharkiv.setRoads(null);
        List<Map<Integer, Integer>> pathsToFind = new ArrayList<>();
        pathsToFind.add(Map.of(0, 3));
        pathsToFind.add(Map.of(1, 3));
        pathsToFind.add(Map.of(2, 4));
        SimpleDijkstra simpleDijkstra = new SimpleDijkstra(List.of(gdansk, bydgoszcz, torun, warszawa, kharkiv), pathsToFind);
        List<Map<Integer, Integer>> result = new ArrayList<>();
        result.add(Map.of(1, 3));
        result.add(Map.of(2, 2));
        result.add(Map.of(3, 0));
        assertResult(simpleDijkstra, result);
    }

    private void assertResult(SimpleDijkstra simpleDijkstra, List<Map<Integer, Integer>> expected) {
        assertEquals(expected, Objects.requireNonNull(DijkstraFromFile.fileParser("/input.txt")).result());
    }
}