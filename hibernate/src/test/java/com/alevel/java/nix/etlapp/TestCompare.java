package com.alevel.java.nix.etlapp;

import com.alevel.java.nix.etlapp.model.Database;
import com.alevel.java.nix.pathfinder.entity.Cities;
import com.alevel.java.nix.pathfinder.entity.Connection;
import com.alevel.java.nix.pathfinder.entity.ConnectionId;
import com.alevel.java.nix.pathfinder.entity.Problems;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCompare {

    public Database initialize() {
        Cities gdansk = new Cities(1L, "gdansk");
        Cities bydgoszcz = new Cities(2L, "bydgoszcz");
        Cities torun = new Cities(3L, "torun");
        Cities warszawa = new Cities(4L, "warszawa");
        Cities kharkiv = new Cities(5L, "kharkiv");
        List<Cities> cities = List.of(gdansk, bydgoszcz, torun, warszawa, kharkiv);
        List<Connection> connections = List.of(new Connection(new ConnectionId(gdansk, bydgoszcz, 1))
                , new Connection(new ConnectionId(gdansk, torun, 3)), new Connection(new ConnectionId(bydgoszcz, gdansk, 1))
                , new Connection(new ConnectionId(bydgoszcz, torun, 1)), new Connection(new ConnectionId(bydgoszcz, warszawa, 4))
                , new Connection(new ConnectionId(torun, gdansk, 3)), new Connection(new ConnectionId(torun, bydgoszcz, 1))
                , new Connection(new ConnectionId(torun, warszawa, 1)), new Connection(new ConnectionId(warszawa, bydgoszcz, 4))
                , new Connection(new ConnectionId(warszawa, torun, 1)));
        Problems problem1 = new Problems(1L, gdansk, warszawa);
        Problems problem2 = new Problems(2L, bydgoszcz, warszawa);
        Problems problem3 = new Problems(3L, torun, kharkiv);
        List<Problems> problems = List.of(problem1, problem2, problem3);
        return new Database(cities, connections, problems);
    }


    public void assertDatabases(Database expected, Database actual) {

        for (int i = 0, length = expected.getCities().size(); i < length; i++) {
            assertEquals(expected.getCities().get(i).getId(), actual.getCities().get(i).getId());
            assertEquals(expected.getCities().get(i).getName(), actual.getCities().get(i).getName());
        }

        for (int i = 0, length = expected.getConnections().size(); i < length; i++) {
            assertEquals(expected.getConnections().get(i).getId().getFromCity().getName(), actual.getConnections().get(i).getId().getFromCity().getName());
            assertEquals(expected.getConnections().get(i).getId().getToCity().getName(), actual.getConnections().get(i).getId().getToCity().getName());
            assertEquals(expected.getConnections().get(i).getId().getCost(), actual.getConnections().get(i).getId().getCost());
        }

        for (int i = 0, length = expected.getProblems().size(); i < length; i++) {
            assertEquals(expected.getProblems().get(i).getFromCity().getName(), actual.getProblems().get(i).getFromCity().getName());
            assertEquals(expected.getProblems().get(i).getToCity().getName(), actual.getProblems().get(i).getToCity().getName());
        }

    }
}
