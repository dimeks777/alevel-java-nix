package com.alevel.java.nix.etlapp.output;

import com.alevel.java.nix.etlapp.TestCompare;
import com.alevel.java.nix.etlapp.input.FileParser;
import com.alevel.java.nix.etlapp.model.Database;
import com.alevel.java.nix.pathfinder.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;


class DatabaseLoaderTest {

    private final TestCompare testCompare = new TestCompare();

    @Test
    public void load() {

        Configuration cfg = new Configuration().configure();
        try (SessionFactory sessionFactory = cfg.buildSessionFactory();
             Session session = sessionFactory.openSession()
        ) {
            Database database = FileParser.getDatabaseFromResourceFile("/input.txt");
            DatabaseLoader.load(database, session);

            List<Cities> actualCities = session
                    .createQuery("from Cities ", Cities.class)
                    .list();
            List<Connection> actualConnections = session
                    .createQuery("from Connection", Connection.class)
                    .list();
            List<Problems> actualProblems = session
                    .createQuery("from Problems", Problems.class)
                    .list();

            actualConnections.sort(Comparator.comparingLong(p -> p.getId().getFromCity().getId()));
            Database actual = new Database(actualCities, actualConnections, actualProblems);
            testCompare.assertDatabases(testCompare.initialize(), actual);
        }
    }
}