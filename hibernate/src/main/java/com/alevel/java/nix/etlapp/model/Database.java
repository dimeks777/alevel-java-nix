package com.alevel.java.nix.etlapp.model;

import com.alevel.java.nix.pathfinder.entity.Cities;
import com.alevel.java.nix.pathfinder.entity.Connection;
import com.alevel.java.nix.pathfinder.entity.Problems;

import java.util.List;

public class Database {
    private final List<Cities> cities;
    private final List<Connection> connections;
    private final List<Problems> problems;

    public Database(List<Cities> cities, List<Connection> connections, List<Problems> problems) {
        this.cities = cities;
        this.connections = connections;
        this.problems = problems;
    }

    public List<Cities> getCities() {
        return cities;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public List<Problems> getProblems() {
        return problems;
    }
}
