package com.alevel.java.nix.pathfinder.entity;

import javax.persistence.*;

@Entity
@Table(name = "impossible_routes")
public class ImpossibleRoutes {

    @Id
    @Column(name = "problem_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "problem_id")
    @MapsId("id")
    private Problems problem;

    public ImpossibleRoutes() {
    }

    public ImpossibleRoutes(Long id, Problems problem) {
        this.id = id;
        this.problem = problem;
    }

    public Problems getProblem() {
        return problem;
    }

    public void setProblem(Problems problem) {
        this.problem = problem;
    }
}
