package com.alevel.java.nix.pathfinder.entity;

import javax.persistence.*;

@Entity
@Table(name = "problems")
public class Problems {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "problem_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "from_city")
    private Cities fromCity;

    @ManyToOne
    @JoinColumn(name = "to_city")
    private Cities toCity;

    public Problems() {

    }

    public Problems(Long id, Cities fromCity, Cities toCity) {
        this.id = id;
        this.fromCity = fromCity;
        this.toCity = toCity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cities getFromCity() {
        return fromCity;
    }

    public void setFromCity(Cities fromCity) {
        this.fromCity = fromCity;
    }

    public Cities getToCity() {
        return toCity;
    }

    public void setToCity(Cities toCity) {
        this.toCity = toCity;
    }
}
