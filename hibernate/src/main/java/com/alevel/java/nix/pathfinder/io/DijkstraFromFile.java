package com.alevel.java.nix.pathfinder.io;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alevel.java.nix.tasks.task3.City;
import com.alevel.java.nix.dijkstra.SimpleDijkstra;
import com.alevel.java.nix.tasks.task3.Road;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class DijkstraFromFile {

    private static final Logger logger = LoggerFactory.getLogger(DijkstraFromFile.class);

    public static SimpleDijkstra fileParser(String path) {
        List<String> data = null;
        try {
            Path fileURL = Path.of(DijkstraFromFile.class.getResource(path).toURI());
            data = Files.readAllLines(fileURL);
        } catch (Exception e) {
            logger.error("Cannot read file {} : {}", path, e.getMessage());
        }

        if (data != null) {

            int size = Integer.parseInt(data.get(0));
            List<City> cities = new ArrayList<>(size);
            HashMap<String, Integer> names = new HashMap<>();
            for (int i = 1; i < size + 1; i++) {
                cities.add(new City(i, null));
            }
            int step;
            int last = 0;

            for (int i = 1, count = 0; i < data.size() && count < size; i += step, count++) {

                String temp = data.get(i);
                int countOfRoads = Integer.parseInt(data.get(i + 1));
                step = countOfRoads + 2;
                List<Road> edges = new ArrayList<>();
                for (int j = 0; j < countOfRoads; j++) {
                    String[] tempEdges = data.get(i + 2 + j).split(" ");
                    edges.add(new Road(cities.get(Integer.parseInt(tempEdges[0]) - 1), Integer.parseInt(tempEdges[1])));
                }
                cities.get(count).setRoads(edges);
                cities.get(count).setName(temp);
                names.put(temp, count);
                if (count == size - 1) {
                    last = i + step;
                }
            }

            int neededPaths = Integer.parseInt(data.get(last));
            List<Map<Integer, Integer>> routes = new ArrayList<>();
            for (int i = 1; i < neededPaths + 1; i++) {
                String[] srcDest = data.get(last + i).split(" ");
                routes.add(Map.of(names.get(srcDest[0]), names.get(srcDest[1])));
            }
            return new SimpleDijkstra(cities, routes);
        }
        return null;
    }

}
