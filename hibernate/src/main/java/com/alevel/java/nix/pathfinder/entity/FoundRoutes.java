package com.alevel.java.nix.pathfinder.entity;

import javax.persistence.*;

@Entity
@Table(name = "found_routes")
public class FoundRoutes {

    @Id
    @Column(name = "problem_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "problem_id")
    @MapsId("id")
    private Problems problem;

    @Column(name = "cost")
    private Integer minCost;

    public FoundRoutes() {
    }

    public FoundRoutes(Long id, Problems problem, Integer minCost) {
        this.id = id;
        this.problem = problem;
        this.minCost = minCost;
    }

    public Problems getProblem() {
        return problem;
    }

    public void setProblem(Problems problem) {
        this.problem = problem;
    }

    public Integer getMinCost() {
        return minCost;
    }

    public void setMinCost(Integer minCost) {
        this.minCost = minCost;
    }
}
