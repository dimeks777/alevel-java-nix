package com.alevel.java.nix.etlapp.output;

import com.alevel.java.nix.etlapp.model.Database;
import com.alevel.java.nix.pathfinder.entity.Cities;
import com.alevel.java.nix.pathfinder.entity.Connection;
import com.alevel.java.nix.pathfinder.entity.Problems;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DatabaseLoader {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);

    public static void load(Database database, Session session) {
        try {
            session.beginTransaction();
            for (Cities city : database.getCities()) {
                session.save(city);
            }

            for (Connection connection : database.getConnections()) {
                session.save(connection);
            }

            for (Problems problem : database.getProblems()) {
                session.save(problem);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while saving cities");
            session.getTransaction().rollback();
        }
    }
}
