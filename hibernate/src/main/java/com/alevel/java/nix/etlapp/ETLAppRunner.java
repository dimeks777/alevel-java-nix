package com.alevel.java.nix.etlapp;

import com.alevel.java.nix.etlapp.input.FileParser;
import com.alevel.java.nix.etlapp.model.Database;
import com.alevel.java.nix.etlapp.output.DatabaseLoader;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class ETLAppRunner {
    public static void main(String[] args) {
        Configuration cfg = new Configuration().configure(new File(ETLAppRunner.class.getResource("/etlappHibernate.cfg.xml").getPath()));
        try (SessionFactory sessionFactory = cfg.buildSessionFactory();
             Session session = sessionFactory.openSession()
        ) {
            Database database = FileParser.getDatabaseFromResourceFile("/input.txt");
            DatabaseLoader.load(database, session);
        }
    }
}
