package com.alevel.java.nix.pathfinder;

import com.alevel.java.nix.dijkstra.SimpleDijkstra;
import com.alevel.java.nix.pathfinder.entity.*;
import com.alevel.java.nix.pathfinder.io.DijkstraFromFile;
import com.alevel.java.nix.tasks.task3.City;
import com.alevel.java.nix.tasks.task3.Road;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HibernateDijkstraApp {
    private static final Logger logger = LoggerFactory.getLogger(DijkstraFromFile.class);

    public static void main(String[] args) {
        run("/input.txt");
    }

    public static void run(String pathToResource) {
        Configuration cfg = new Configuration().configure();
        try (SessionFactory sessionFactory = cfg.buildSessionFactory();
             Session session = sessionFactory.openSession()
        ) {
            SimpleDijkstra simpleDijkstra = DijkstraFromFile.fileParser(pathToResource);
            if (simpleDijkstra != null) {
                List<Map<Integer, Integer>> list = simpleDijkstra.result();
                writeEntities(session, simpleDijkstra, list);
            }
        }
    }

    public static void writeEntities(Session session, SimpleDijkstra citiesAndConnections,
                                     List<Map<Integer, Integer>> resultList) {
        Map<Long, Cities> cities = new HashMap<>();

        try {
            session.beginTransaction();
            for (City city : citiesAndConnections.getCities()) {
                Cities temp = new Cities(city.getId(), city.getName());
                cities.put(city.getId(), temp);
                session.save(temp);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction");
            session.getTransaction().rollback();
        }

        long id = 1;
        Map<Long, Problems> problemMap = new LinkedHashMap<>();

        try {
            session.beginTransaction();
            for (City city : citiesAndConnections.getCities()) {
                for (Road connection : city.getRoads()) {
                    Connection temp = new Connection(new ConnectionId(cities.get(city.getId()),
                            cities.get(connection.getDest().getId()), connection.getCost()));
                    session.save(temp);
                }
            }

            for (Map<Integer, Integer> route : citiesAndConnections.getNeededPaths()) {
                for (var entry : route.entrySet()) {
                    Problems temp = new Problems(id, cities.get((long) entry.getKey() + 1), cities.get((long) entry.getValue() + 1));
                    problemMap.put(id++, temp);
                    session.save(temp);
                }
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            session.getTransaction().rollback();
        }

        try {
            session.beginTransaction();
            for (Map<Integer, Integer> map : resultList) {
                for (var entry : map.entrySet()) {
                    int temp;
                    id = (long) entry.getKey();
                    if ((temp = entry.getValue()) != 0) {
                        session.save(new FoundRoutes(id, problemMap.get(id), temp));
                    } else {
                        session.save(new ImpossibleRoutes(id, problemMap.get(id)));
                    }
                }
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("Error while executing transaction");
            session.getTransaction().rollback();
        }
    }
}
