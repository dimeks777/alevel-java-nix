package com.alevel.java.nix.pathfinder.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "connection")
public class Connection {

    @EmbeddedId
    private ConnectionId id;

    public Connection() {
        id = new ConnectionId();
    }

    public Connection(ConnectionId id) {
        this.id = id;
    }

    public ConnectionId getId() {
        return id;
    }

    public void setId(ConnectionId id) {
        this.id = id;
    }
}
