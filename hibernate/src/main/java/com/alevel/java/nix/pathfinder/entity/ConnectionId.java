package com.alevel.java.nix.pathfinder.entity;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ConnectionId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "from_city")
    private Cities fromCity;

    @ManyToOne
    @JoinColumn(name = "to_city")
    private Cities toCity;

    private Integer cost;

    public ConnectionId() {
    }

    public ConnectionId(Cities fromCity, Cities toCity, Integer cost) {
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.cost = cost;
    }

    public Cities getFromCity() {
        return fromCity;
    }

    public void setFromCity(Cities from) {
        this.fromCity = from;
    }

    public Cities getToCity() {
        return toCity;
    }

    public void setToCity(Cities to) {
        this.toCity = to;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
