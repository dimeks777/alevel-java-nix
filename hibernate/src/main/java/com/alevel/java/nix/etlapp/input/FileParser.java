package com.alevel.java.nix.etlapp.input;

import com.alevel.java.nix.etlapp.model.Database;
import com.alevel.java.nix.pathfinder.entity.Cities;
import com.alevel.java.nix.pathfinder.entity.Connection;
import com.alevel.java.nix.pathfinder.entity.ConnectionId;
import com.alevel.java.nix.pathfinder.entity.Problems;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class FileParser {
    private static final Logger logger = LoggerFactory.getLogger(FileParser.class);

    public static Database getDatabaseFromResourceFile(String pathToResource) {

        List<String> data = null;
        try {
            Path fileURL = Path.of(FileParser.class.getResource(pathToResource).toURI());
            data = Files.readAllLines(fileURL);
        } catch (Exception e) {
            logger.error("Cannot read file {} : {}", pathToResource, e.getMessage());
        }


        if (data != null) {


            int citiesCount = Integer.parseInt(data.get(0));
            List<Cities> cities = new ArrayList<>(citiesCount);
            HashMap<String, Integer> names = new LinkedHashMap<>();
            List<Connection> connections = new ArrayList<>();
            List<Problems> problems = new ArrayList<>();

            for (long id = 1; id < citiesCount + 1; id++) {
                cities.add(new Cities(id, null));
            }

            int linesBeforeNextCity;
            int last = 0;

            for (int i = 1, count = 0; i < data.size() && count < citiesCount; i += linesBeforeNextCity, count++) {

                String temp = data.get(i);
                int countOfRoads = Integer.parseInt(data.get(i + 1));
                linesBeforeNextCity = countOfRoads + 2;

                for (int j = 0; j < countOfRoads; j++) {
                    String[] tempEdges = data.get(i + 2 + j).split(" ");
                    connections.add(new Connection(new ConnectionId(cities.get(count), cities.get(Integer.parseInt(tempEdges[0]) - 1), Integer.parseInt(tempEdges[1]))));
                }

                cities.get(count).setName(temp);
                names.put(temp, count);

                if (count == citiesCount - 1) {
                    last = i + linesBeforeNextCity;
                }
            }

            int problemCount = Integer.parseInt(data.get(last));

            for (int i = 1; i < problemCount + 1; i++) {
                String[] srcDest = data.get(last + i).split(" ");
                problems.add(new Problems((long) i, cities.get(names.get(srcDest[0])), cities.get(names.get(srcDest[1]))));
            }
            return new Database(cities, connections, problems);
        }
        return null;
    }
}
